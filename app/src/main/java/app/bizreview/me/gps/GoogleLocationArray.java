package app.bizreview.me.gps;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import app.bizreview.me.R;
import app.bizreview.me.utils.AppUtil;
import app.bizreview.me.utils.Const;
import app.bizreview.me.utils.DebugHelper;


// TODO: Auto-generated Javadoc

/**
 * The Class GoogleLocation.
 */
public class GoogleLocationArray {

	/** The Constant TAG. */
	private static final String TAG = "GoogleLocation";

	/** The Constant SEPERATOR. */
	private static final String SEPERATOR = ",";

	/** The Constant AMP. */
	private static final String AMP = "&";

	/** The parser task. */
	private static ParserTask parserTask;

	// /** The atv places. */
	// private static AutoCompleteTextView atvPlaces;
	//
	/** The places list. */
	private static ListView atvPlaces;

	/** The m context. */
	private static Context mContext;

	/**
	 * Gets the lat lng.
	 * 
	 * @param latlng
	 *            the latlng
	 * @return the lat lng
	 */
	public static LatLng getLatLng(String latlng) {
		String[] arr = latlng.split(SEPERATOR);
		return new LatLng(Double.parseDouble(arr[0]),
				Double.parseDouble(arr[1]));
	}

	/**
	 * Gets the lat lng str from location name.
	 * 
	 * @param callingActivity
	 *            the calling activity
	 * @param loc
	 *            the loc
	 * @return the lat lng str from location name
	 */
	public static String getLatLngStrFromLocationName(Activity callingActivity,
			LatLng loc) {
		double lat = loc.latitude;
		double lng = loc.longitude;
		return lat + SEPERATOR + lng;
	}

	/**
	 * Adds the marker on g map.
	 * 
	 * @param gMap
	 *            the g map
	 * @param latLng
	 *            the lat lng
	 * @param locName
	 *            the loc name
	 * @param drawable
	 *            the drawable
	 */
	public static void addMarkerOnGMap(GoogleMap gMap, LatLng latLng,
                                       String locName, int drawable) {
		gMap.addMarker(new MarkerOptions().position(latLng).title(locName)
				.icon(BitmapDescriptorFactory.fromResource(drawable)));
	}

	/**
	 * Gets the and add marker on g map.
	 * 
	 * @param gMap
	 *            the g map
	 * @param latLng
	 *            the lat lng
	 * @param locName
	 *            the loc name
	 * @param drawable
	 *            the drawable
	 * @return the and add marker on g map
	 */
	public static Marker getAndAddMarkerOnGMap(GoogleMap gMap, LatLng latLng,
                                               String locName, int drawable) {
		return gMap.addMarker(new MarkerOptions().position(latLng)
				.title(locName)
				.icon(BitmapDescriptorFactory.fromResource(drawable)));
	}

	/**
	 * Sets the g map camera position.
	 * 
	 * @param gMap
	 *            the g map
	 * @param latLng
	 *            the lat lng
	 */
	public static void setGMapCameraPosition(GoogleMap gMap, LatLng latLng) {
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(latLng).zoom(Const.C_12).tilt(Const.C_30).build();
		gMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));
	}

	/**
	 * Adds the poly line on g map.
	 * 
	 * @param gMap
	 *            the g map
	 * @param startPoint
	 *            the start point
	 * @param endPoint
	 *            the end point
	 * @param startMarker
	 *            the start marker
	 * @param endMarker
	 *            the end marker
	 */
	public static void addPolyLineOnGMap(GoogleMap gMap, LatLng startPoint,
                                         LatLng endPoint, Marker startMarker, Marker endMarker) {
		LatLngBounds.Builder b = new LatLngBounds.Builder();
		b.include(startMarker.getPosition());
		b.include(endMarker.getPosition());
		LatLngBounds bounds = b.build();
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
				Const.C_10);
		gMap.animateCamera(cu);
	}

	/**
	 * Sets the camera position.
	 * 
	 * @param gMap
	 *            the g map
	 * @param startMarker
	 *            the start marker
	 * @param endMarker
	 *            the end marker
	 */
	public static void setCameraPosition(GoogleMap gMap, Marker startMarker,
                                         Marker endMarker) {
		LatLngBounds.Builder b = new LatLngBounds.Builder();
		b.include(startMarker.getPosition());
		b.include(endMarker.getPosition());
		LatLngBounds bounds = b.build();
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
				Const.C_25, Const.C_25, Const.C_10);
		gMap.animateCamera(cu);
	}

	/**
	 * Download url.
	 * 
	 * @param strUrl
	 *            the str url
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.connect();
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (MalformedURLException e) {
			DebugHelper.trackException(TAG, e);
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	/**
	 * The Class PlacesTask.
	 */
	public static class PlacesTask extends AsyncTask<String, Void, String> {

		/** The progress bar. */
		private LinearLayout progressBar;

		/** The flag drop down. */
		private boolean flagDropDown;

		/**
		 * Instantiates a new places task.
		 * 
		 * @param c
		 *            the c
		 * @param atv
		 *            the atv
		 * @param progress
		 *            the progress
		 * @param flag
		 *            the flag
		 */
		public PlacesTask(Context c, ListView atv, LinearLayout progress,
				Boolean flag) {
			mContext = c;
			atvPlaces = atv;
			progressBar = progress;
			flagDropDown = flag;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		public String doInBackground(String... place) {
			String data = "";
			String key = "key=" + "AIzaSyCoVOthcDL1F6Fg2sif_RtwktCw0sNNSKY";

			String input = "";

			try {
				input = "input=" + URLEncoder.encode(place[0], "utf-8");
			} catch (UnsupportedEncodingException e1) {
				DebugHelper.trackException(TAG, e1);
			}
			String types = "types=geocode";
			String sensor = "sensor=false";
			String parameters = input + AMP + types + AMP + sensor + AMP + key;
			String output = "json";
			String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"
					+ output + "?" + parameters;

			if (AppUtil.Companion.isNetworkAvailable(mContext)) {
				try {
					data = downloadUrl(url);
				} catch (IOException e) {
					DebugHelper.trackException(TAG, e);
				}
				try {
					data = downloadUrl(url);
				} catch (IOException e) {
					DebugHelper.trackException(TAG, e);
				}
			}
			return data;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parserTask = new ParserTask(atvPlaces, progressBar, flagDropDown);
			parserTask.execute(result);
		}
	}

	/**
	 * The Class ParserTask.
	 */
	public static class ParserTask extends
			AsyncTask<String, Integer, List<HashMap<String, String>>> {

		/** The j object. */
		private JSONObject jObject;

		/** The progress bar. */
		private LinearLayout progressBar;

		/** The flag drop down. */
		public Boolean flagDropDown;

		/**
		 * Instantiates a new parser task.
		 * 
		 * @param atv
		 *            the atv
		 * @param progress
		 *            the progress
		 * @param flag
		 *            the flag
		 */
		public ParserTask(ListView atv, LinearLayout progress, Boolean flag) {
			atvPlaces = atv;
			progressBar = progress;
			flagDropDown = flag;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		public List<HashMap<String, String>> doInBackground(String... jsonData) {
			List<HashMap<String, String>> places = null;
			if (AppUtil.Companion.isNetworkAvailable(mContext)) {
				PlaceJSONParser placeJsonParser = new PlaceJSONParser();

				try {
					jObject = new JSONObject(jsonData[0]);
					places = placeJsonParser.parse(jObject);

				} catch (JSONException e) {
					DebugHelper.trackException(TAG, e);
				}
			}
			return places;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		public void onPostExecute(List<HashMap<String, String>> result) {
			if (result != null) {
				String[] from = new String[] { "description" };
				int[] to = new int[] { R.id.tv_location };

				SimpleAdapter adapter = new SimpleAdapter(mContext, result,
						R.layout.row_search_location, from, to);
				progressBar.setVisibility(View.INVISIBLE);
				atvPlaces.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		}
	}

	/**
	 * Gets the location label.
	 * 
	 * @param tag
	 *            the tag
	 * @return the location label
	 */
	public static String getLocationLabel(String tag) {
		if (tag.equals("natural_feature")
				|| tag.equals("administrative_area_level_2")
				|| tag.equals("administrative_area_level_1")
				|| tag.equals("administrative_area_level_3")) {
			return tag;
		} else {
			return "locality";
		}
	}
}
