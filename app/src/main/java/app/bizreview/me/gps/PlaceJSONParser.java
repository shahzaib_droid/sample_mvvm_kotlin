package app.bizreview.me.gps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.bizreview.me.utils.DebugHelper;


// TODO: Auto-generated Javadoc

/**
 * The Class PlaceJSONParser.
 */
public class PlaceJSONParser {
 
    /** The Constant TAG. */
    private static final String TAG = "PlaceJSONParser";
    
    /** The Constant DESCRIPTION. */
    private static final String DESCRIPTION = "description";
    
    /** The Constant REFERENCE. */
    private static final String REFERENCE = "reference";
    
    /**
     * Parses the.
     *
     * @param jObject the j object
     * @return the list
     */
    public List<HashMap<String,String>> parse(JSONObject jObject){
 
        JSONArray jPlaces = null;
        try {
            jPlaces = jObject.getJSONArray("predictions");
        } catch (JSONException e) {
            DebugHelper.trackException(e);
        }
        return getPlaces(jPlaces);
    }
 
    /**
     * Gets the places.
     *
     * @param jPlaces the j places
     * @return the places
     */
    private List<HashMap<String, String>> getPlaces(JSONArray jPlaces){
        int placesCount = jPlaces.length();
        List<HashMap<String, String>> placesList = new ArrayList<HashMap<String,String>>();
        HashMap<String, String> place = null;
 
        for(int i=0; i<placesCount;i++){
            try {
                place = getPlace((JSONObject)jPlaces.get(i));
                placesList.add(place);
 
            } catch (JSONException e) {
                DebugHelper.trackException(TAG, e);
            }
        }
 
        return placesList;
    }
 
    /**
     * Gets the place.
     *
     * @param jPlace the j place
     * @return the place
     */
    private HashMap<String, String> getPlace(JSONObject jPlace){
 
        HashMap<String, String> place = new HashMap<String, String>();
 
        String id="";
        String reference="";
        String description="";
 
        try {
 
            description = jPlace.getString(DESCRIPTION);
            id = jPlace.getString("id");
            reference = jPlace.getString(REFERENCE);
 
            place.put(DESCRIPTION, description);
            place.put("_id",id);
            place.put(REFERENCE,reference);
 
        } catch (JSONException e) {
            DebugHelper.trackException(TAG, e);
        }
        return place;
    }
}
