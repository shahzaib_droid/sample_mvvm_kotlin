
package app.bizreview.me.gps;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class StructuredFormatting {

    @SerializedName("main_text")
    private String MainText;
    @SerializedName("main_text_matched_substrings")
    private List<MainTextMatchedSubstring> MainTextMatchedSubstrings;
    @SerializedName("secondary_text")
    private String SecondaryText;
    @SerializedName("secondary_text_matched_substrings")
    private List<SecondaryTextMatchedSubstring> SecondaryTextMatchedSubstrings;

    public String getMainText() {
        return MainText;
    }

    public void setMainText(String mainText) {
        MainText = mainText;
    }

    public List<MainTextMatchedSubstring> getMainTextMatchedSubstrings() {
        return MainTextMatchedSubstrings;
    }

    public void setMainTextMatchedSubstrings(List<MainTextMatchedSubstring> mainTextMatchedSubstrings) {
        MainTextMatchedSubstrings = mainTextMatchedSubstrings;
    }

    public String getSecondaryText() {
        return SecondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        SecondaryText = secondaryText;
    }

    public List<SecondaryTextMatchedSubstring> getSecondaryTextMatchedSubstrings() {
        return SecondaryTextMatchedSubstrings;
    }

    public void setSecondaryTextMatchedSubstrings(List<SecondaryTextMatchedSubstring> secondaryTextMatchedSubstrings) {
        SecondaryTextMatchedSubstrings = secondaryTextMatchedSubstrings;
    }

}
