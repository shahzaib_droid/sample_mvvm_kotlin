
package app.bizreview.me.gps;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class Prediction {

    @SerializedName("description")
    private String Description;
    @SerializedName("id")
    private String Id;
    @SerializedName("matched_substrings")
    private List<MatchedSubstring> MatchedSubstrings;
    @SerializedName("place_id")
    private String PlaceId;
    @SerializedName("reference")
    private String Reference;
    @SerializedName("structured_formatting")
    private app.bizreview.me.gps.StructuredFormatting StructuredFormatting;
    @SerializedName("terms")
    private List<Term> Terms;
    @SerializedName("types")
    private List<String> Types;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public List<MatchedSubstring> getMatchedSubstrings() {
        return MatchedSubstrings;
    }

    public void setMatchedSubstrings(List<MatchedSubstring> matchedSubstrings) {
        MatchedSubstrings = matchedSubstrings;
    }

    public String getPlaceId() {
        return PlaceId;
    }

    public void setPlaceId(String placeId) {
        PlaceId = placeId;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public app.bizreview.me.gps.StructuredFormatting getStructuredFormatting() {
        return StructuredFormatting;
    }

    public void setStructuredFormatting(app.bizreview.me.gps.StructuredFormatting structuredFormatting) {
        StructuredFormatting = structuredFormatting;
    }

    public List<Term> getTerms() {
        return Terms;
    }

    public void setTerms(List<Term> terms) {
        Terms = terms;
    }

    public List<String> getTypes() {
        return Types;
    }

    public void setTypes(List<String> types) {
        Types = types;
    }

}
