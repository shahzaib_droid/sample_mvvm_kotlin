package app.bizreview.me.gps;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


import java.util.ArrayList;

public abstract class BaseAdapter extends ArrayAdapter<Object> {

	private final String TAG = this.getClass().getName();

	protected Context mContext = null;

	protected ArrayList<Object> mAllData = null;

	protected LayoutInflater mInflater = null;

	protected Handler mScreenHandler = null;

	BaseAdapter(Context context, int resource, ArrayList<Object> objects) {
		super(context, 0, objects);
		initData(context, objects);
	}

	void initData(final Context mContext, final ArrayList<Object> mDataList) {
		this.mContext = mContext;
		this.mAllData = mDataList;
		if (mContext != null) {
			this.mInflater = (LayoutInflater) this.mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
	}

	public Context getContext() {
		return this.mContext;
	}

	public void setContext(final Context mContext) {
		this.mContext = mContext;
	}

	public ArrayList<Object> getData() {
		return this.mAllData;
	}

	public void setData(final ArrayList<Object> mDataList) {
		if(this.mAllData != null)
			this.mAllData = new ArrayList<Object>();
		this.mAllData = mDataList;
	}

	public LayoutInflater getInflater() {
		return this.mInflater;
	}

	public void setInflater(final LayoutInflater mInflater) {
		this.mInflater = mInflater;
	}

	public String getTAG() {
		return this.TAG;
	}

	public void appendData(final ArrayList<Object> mDataList) {
		if (mDataList != null && !mDataList.isEmpty()) {
			if (mAllData == null) {
				mAllData = mDataList;
			} else {
				mAllData.addAll(mDataList);
			}
		}
		notifyDataSetChanged();
	}

	public Handler getScreenHandler() {
		return this.mScreenHandler;
	}

	public void setScreenHandler(final Handler mScreenHandler) {
		this.mScreenHandler = mScreenHandler;
	}

	public void setHanlder(final Handler handler) {
		this.setScreenHandler(this.mScreenHandler);
	}

	@Override
	public int getCount() {

		if (this.mAllData != null) {
			return this.mAllData.size();
		}
		return 0;
	}

	@Override
	public Object getItem(final int index) {
		if (this.mAllData != null) {
			return this.mAllData.get(index);
		}
		return null;
	}

	@Override
	public long getItemId(final int index) {

		return 0;
	}

	public void loadImage(ImageView imageView, String imageURL) {
		// UtilityImage.getThumbnail(imageView,
		// R.drawable.ic_profile_large_rounded, imageURL, mContext);
	}
}
