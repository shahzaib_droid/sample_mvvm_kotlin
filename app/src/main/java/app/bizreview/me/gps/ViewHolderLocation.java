package app.bizreview.me.gps;

import android.view.View;
import android.widget.TextView;

import app.bizreview.me.R;


public class ViewHolderLocation extends BaseViewHolder {

    private View row;
    private TextView mTvName;
    private TextView mTvLocation;


    public ViewHolderLocation(View view) {
        super(null, view);
    }

    protected void initComponents(View view) {
        super.initComponents(view);
        row = view;
        mTvName = (TextView) row.findViewById(R.id.tv_name);
        mTvLocation = (TextView) row.findViewById(R.id.tv_location);
    }

    public void updateView(Prediction data) {
        mTvName.setText(data.getDescription());
        int lastIndex = data.getDescription().lastIndexOf(",");
        mTvLocation.setText(data.getDescription().substring(lastIndex + 2, data.getDescription().length()));
    }
}