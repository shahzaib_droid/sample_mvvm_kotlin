package app.bizreview.me.gps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import app.bizreview.me.R;

public class LocationAdapter extends ArrayAdapter<Prediction> {

    private Context context;
    private List<Prediction> mAllData;

    public LocationAdapter(final Context mContext,
                           final List<Prediction> mDataList) {
        super(mContext, R.layout.row_search_location, mDataList);
        mAllData = mDataList;
        context = mContext;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {
        View rowView = convertView;
        ViewHolderLocation holder = null;
        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(R.layout.row_search_location, parent, false);
            holder = new ViewHolderLocation(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolderLocation) rowView.getTag();
        }
        Prediction data = (Prediction) mAllData.get(position);
        holder.updateView(data);
        return rowView;
    }

}
