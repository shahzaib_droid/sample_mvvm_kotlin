
package app.bizreview.me.gps;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Term {

    @SerializedName("offset")
    private Long Offset;
    @SerializedName("value")
    private String Value;

    public Long getOffset() {
        return Offset;
    }

    public void setOffset(Long offset) {
        Offset = offset;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

}
