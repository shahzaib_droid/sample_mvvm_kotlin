
package app.bizreview.me.gps;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MatchedSubstring {

    @SerializedName("length")
    private Long Length;
    @SerializedName("offset")
    private Long Offset;

    public Long getLength() {
        return Length;
    }

    public void setLength(Long length) {
        Length = length;
    }

    public Long getOffset() {
        return Offset;
    }

    public void setOffset(Long offset) {
        Offset = offset;
    }

}
