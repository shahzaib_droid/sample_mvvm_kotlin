
package app.bizreview.me.gps;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class PredictionsResponse extends Object{

    @SerializedName("predictions")
    private List<Prediction> Predictions;
    @SerializedName("status")
    private String Status;

    public List<Prediction> getPredictions() {
        return Predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        Predictions = predictions;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
