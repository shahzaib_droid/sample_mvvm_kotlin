package app.bizreview.me.gps;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author Shahzaib
 *         <p/>
 *         This is base class of View holder. It has common function for view
 *         holder that every view holder will implement by overriding that
 *         function
 */
class BaseViewHolder implements OnClickListener {
    protected Context mContext;

    BaseViewHolder(Context context, View view) {
        mContext = context;
        initComponents(view);
        addListener();
    }

    protected void initComponents(View view) {
        if (view != null && mContext == null) {
            mContext = view.getContext();
        }
    }

    protected void addListener() {
    }

    @Override
    public void onClick(View v) {
        // Child classes that require this function will override it.
    }

}
