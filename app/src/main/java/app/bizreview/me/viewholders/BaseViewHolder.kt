package app.bizreview.me.viewholders

import android.content.Context
import android.view.View
import android.view.View.OnClickListener

/**
 * @author Arslan Anwar
 *
 *
 * This is base class of View holder. It has common function for view
 * holder that every view holder will implement by overriding that
 * function
 */
open class BaseViewHolder(protected var mContext: Context?, view: View) : OnClickListener {

    init {
        initComponents(view)
        addListener()
    }

    protected fun initComponents(view: View?) {
        if (view != null && mContext == null) {
            mContext = view.context
        }
    }

    protected fun addListener() {}

    override fun onClick(v: View) {
        // Child classes that require this function will override it.
    }

}
