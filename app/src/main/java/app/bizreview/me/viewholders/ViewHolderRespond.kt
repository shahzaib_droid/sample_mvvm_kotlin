package app.bizreview.me.viewholders

import android.content.Context
import android.provider.ContactsContract
import android.support.v7.widget.RecyclerView
import app.bizreview.me.data.model.contacts.Contact
import app.bizreview.me.data.model.sentReviews.Data
import app.bizreview.me.databinding.RowRespondBinding
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by shahzaib on 3/20/2018.
 */
class ViewHolderRespond(val binding: RowRespondBinding, val context: Context?) : RecyclerView.ViewHolder(binding.root) {

    /*
     View holder for respond item list
     */
    var map: MutableMap<String?, String?>? = null

    init {
        map = getContacts()
    }

    fun updateView(review: Data?) {
        binding.tvRate.text = "${review?.rating}/5"
        if (map?.contains(review?.to)!!) {
            binding.tvNumber.text = map?.get(review?.to)
        } else {
            binding.tvNumber.text = review?.to
        }
        binding.tvQuestion.text = review?.response
        binding.tvTime.text = getCorrectTimeFormat(review?.timestamp_responded)
        binding.rbRating.setRating(review?.rating?.toFloat()!!)
    }

    private fun getCorrectTimeFormat(time: String?): String? {
        val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val appFormat = SimpleDateFormat("MMM dd, hh:mm a", Locale.US)

        val date = serverFormat.parse(time)
        return appFormat.format(date)
    }


    fun getContacts(): MutableMap<String?, String?> {
        //        JSONArray jsonArray = new JSONArray();
        //        List arrList = new ArrayList<>();

        var map = mutableMapOf<String?, String?>()

        val projection = arrayOf(ContactsContract.Data.MIMETYPE, ContactsContract.Data.CONTACT_ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Contactables.DATA, ContactsContract.CommonDataKinds.Contactables.TYPE)
        val selection = ContactsContract.Data.MIMETYPE + " in (?, ?)"
        val selectionArgs = arrayOf(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
        val sortOrder = ContactsContract.Contacts.SORT_KEY_ALTERNATIVE

        val uri = ContactsContract.CommonDataKinds.Contactables.CONTENT_URI
        // we could also use Uri uri = ContactsContract.Data.CONTENT_URI;

        // ok, let's work...
        val cursor = context?.getContentResolver()?.query(uri, projection, selection, selectionArgs, sortOrder)

        val mimeTypeIdx = cursor!!.getColumnIndex(ContactsContract.Data.MIMETYPE)
        val idIdx = cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)
        val nameIdx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
        val dataIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.DATA)
        val typeIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Contactables.TYPE)

        while (cursor.moveToNext()) {
            val id = cursor.getLong(idIdx)
            val type = cursor.getInt(typeIdx)
            val data = cursor.getString(dataIdx)
            val mimeType = cursor.getString(mimeTypeIdx)
            if (mimeType == ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE) {
                // mimeType == ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE

                val addressBookContact = Contact(cursor.getString(nameIdx), data)
                var phoneNumber  = addressBookContact.phone?.replace("+", "");
                phoneNumber  = phoneNumber?.replace(" ", "");
                phoneNumber  = phoneNumber?.replace("-", "");
                map.put(phoneNumber, addressBookContact.name)
            }
        }
        cursor.close()
        return map
    }
}