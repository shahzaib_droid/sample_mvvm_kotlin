package app.bizreview.me.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import app.bizreview.me.App
import app.bizreview.me.R

class ImageViewRoundedStroke : ImageView {

    /*
     Rounded image with stroke color on borders
     */
    private var borderWidth = 2
    private var viewWidth: Int = 0
    private var viewHeight: Int = 0
    private var image: Bitmap? = null
    private var paint: Paint? = null
    private var paintBorder: Paint? = null
    private var shader: BitmapShader? = null

    constructor(context: Context) : super(context) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setup()
    }

    constructor(context: Context, attrs: AttributeSet,
                defStyle: Int) : super(context, attrs, defStyle) {
        setup()
    }

    private fun setup() {
        // init paint
        paint = Paint()
        paint!!.isAntiAlias = true

        paintBorder = Paint()
        setBorderColor(Color.WHITE)
        background = ColorDrawable(App.getAppResources().getColor(R.color.white_transparent))
        paintBorder!!.isAntiAlias = true
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, paintBorder)
        paintBorder!!.setShadowLayer(2.0f, 0.0f, 1.0f, Color.BLACK)
    }

    fun setBorderWidth(borderWidth: Int) {
        this.borderWidth = borderWidth
        this.invalidate()
    }

    fun setBorderColor(borderColor: Int) {
        if (paintBorder != null)
            paintBorder!!.color = borderColor

        this.invalidate()
    }

    private fun loadBitmap() {
        val bitmapDrawable = this.drawable as BitmapDrawable

        if (bitmapDrawable != null)
            image = bitmapDrawable.bitmap
    }

    @SuppressLint("DrawAllocation")
    public override fun onDraw(canvas: Canvas) {
        // load the bitmap
        loadBitmap()

        // init shader
        if (image != null) {
            shader = BitmapShader(Bitmap.createScaledBitmap(image!!,
                    canvas.width, canvas.height, false),
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            paint!!.shader = shader
            val circleCenter = viewWidth / 2

            // circleCenter is the x or y of the view's center
            // radius is the radius in pixels of the cirle to be drawn
            // paint contains the shader that will texture the shape
            canvas.drawCircle((circleCenter + borderWidth).toFloat(), (circleCenter + borderWidth).toFloat(), circleCenter + borderWidth - 4.0f,
                    paintBorder!!)
            canvas.drawCircle((circleCenter + borderWidth).toFloat(), (circleCenter + borderWidth).toFloat(), circleCenter - 4.0f, paint!!)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = measureWidth(widthMeasureSpec)
        val height = measureHeight(heightMeasureSpec, widthMeasureSpec)

        viewWidth = width - borderWidth * 2
        viewHeight = height - borderWidth * 2

        setMeasuredDimension(width, height)
    }

    private fun measureWidth(measureSpec: Int): Int {
        var result = 0
        val specMode = View.MeasureSpec.getMode(measureSpec)
        val specSize = View.MeasureSpec.getSize(measureSpec)

        if (specMode == View.MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize
        } else {
            // Measure the text
            result = viewWidth
        }

        return result
    }

    private fun measureHeight(measureSpecHeight: Int, measureSpecWidth: Int): Int {
        var result = 0
        val specMode = View.MeasureSpec.getMode(measureSpecHeight)
        val specSize = View.MeasureSpec.getSize(measureSpecHeight)

        if (specMode == View.MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize
        } else {
            // Measure the text (beware: ascent is a negative number)
            result = viewHeight
        }

        return result + 2
    }
}