package app.bizreview.me.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import app.bizreview.me.App;
import app.bizreview.me.R;

@SuppressWarnings("deprecation")
public class UtilityImageLoader {

    public static DisplayImageOptions mOptions;

    public static void showRoundedImageWithNoBackground(
            final ImageView mIvImage, String url) {
        setRoundedOptionsWithLoader();
        ImageLoader.getInstance().displayImage(url, mIvImage, mOptions,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        ((ImageView) view).setBackgroundResource(0);
                        ((ImageView) view).setImageBitmap(loadedImage);
                        if (loadedImage == null) {
                            ((ImageView) view).setImageResource(R.drawable.ic_launcher);
                        }
                    }
                });
    }

    public static void showImageWithProgressBar(final ImageView mIvImage,
                                                String url, final LinearLayout progressBar) {
        setOptionsWithOutLoader();
        ImageLoader.getInstance().displayImage(url, mIvImage, mOptions,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        ((ImageView) view).setBackgroundResource(0);
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private static DisplayImageOptions setRoundedOptionsWithLoader() {
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .showImageOnLoading(R.drawable.ic_launcher)
                .considerExifParams(true)
//                .displayer(
//                        new CircleBitmapDisplayer(App.Companion
//                                .getAppResources().getColor(R.color.white), 1))
                .resetViewBeforeLoading(true).build();

        return mOptions;
    }

    private static DisplayImageOptions setOptionsWithOutLoader() {
        mOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .considerExifParams(true).resetViewBeforeLoading(true).build();

        return mOptions;
    }

}
