package app.bizreview.me.utils


import okhttp3.MediaType
import okhttp3.RequestBody

/**
 * Created by shahzaib on 1/5/2016.
 */
object NetworkUtility {


    fun toRequestBody(value: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), value)
    }
}
