package app.bizreview.me.utils

/**
 * Created by shahzaib on 3/5/2018.
 */
object AppConstants {
    // shared pref constants

    const val KEY_IS_DRAWER_HOME = "is_drawer_home"
    const val KEY_IS_LOGGED = "is_logged"
    const val KEY_USER_ID = "user_id"
    const val KEY_EMAIL = "email"
    const val KEY_FULL_NAME = "fullname"
    const val KEY_PHONE = "phone"
    const val KEY_FIRST_NAME = "first_name"
    const val KEY_LAST_NAME = "last_name"
    const val KEY_USER_PASSWORD = "user_password"
    const val KEY_USER_TOKEN_EXPIRE = "KEY_USER_TOKEN_EXPIRE"

    const val URL_APPLY = "https://app.bizreview.me/auth/signup";
    const val URL_APPLY_TITLE = "Sign Up";

}