/**
 *
 */

package app.bizreview.me.utils;

public enum AppBundles {

    // Enums for usage as keys

    USER("user"), PLACE("place"), CROPPER_IMAGE("cropper_image"), LINK("link"), NETWORK_LIST("network_list"), RESULT_LIST("result_list"), SSIDS_COUNT("ssids_count"), UPLOAD_RESULTS("upload_results"),
    MAC("mac"), FROM_MAC("from_mac"), TEST_COUNT("test_count"), DIFF_TIME("diff_time"), START_TIME("start_time"), BSSID("bssid"),
    TEST_RESULTS("test_results"), TEST_TIME("test_time"), BUILDING_DATA("building_data"), BUILDINGS("buildings"), CHECKED("checked"), IS_VISIBLE("is_visible"), FLOOR_DATA("floor_data"), NAME("name"), POSITION("position"), CONFIGURATION("configuration"),
    LAT("lat"), LON("lon"), PLACE_ID("place_id"), ACCESS_DATA("access_data"), DEVICE_DATA("device_data"), IS_EDIT("is_edit"), IS_MANUALLY("is_manually"),
    SSID("ssid"), ID("id"), STRENGTH("strength"), PASSWORD("passowrd"), COMMANDS("commands"), ENC("enc"), SSID_DATA("ssid_data"), HOST_NAME("host_name"), USER_NAME("user_name"), URL("url"), TITLE("title");

    private String _message_key = "";


    AppBundles(String action) {
        this._message_key = action;
    }

    public String getKey() {
        return this._message_key;
    }
}
