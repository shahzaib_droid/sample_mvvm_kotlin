package app.bizreview.me.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.graphics.*
import android.graphics.Bitmap.Config
import android.graphics.BitmapFactory.Options
import android.graphics.PorterDuff.Mode
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Environment
import android.telephony.TelephonyManager
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import android.view.inputmethod.InputMethodManager
import android.widget.*
import java.io.*
import java.net.URI
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.*
import java.util.*
import java.util.regex.Pattern
import java.util.zip.ZipFile

/**
 * The Class AppUtil. Contains all common methods
 */

class AppUtil {

    fun getBitmapFromURI(mContext: Context, uri: Uri): Bitmap? {
        val inputStream: InputStream?
        var bitmap: Bitmap? = null
        try {
            inputStream = mContext.contentResolver.openInputStream(uri)

            val imageOpts = Options()
            imageOpts.inSampleSize = 3
            bitmap = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeStream(inputStream, null, imageOpts),
                    400, 265, false)
            if (bitmap!!.width > 800) {
                bitmap = Bitmap.createScaledBitmap(bitmap, 800,
                        bitmap.height, false)
            }
            if (bitmap!!.height > 600) {
                bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.width,
                        600, false)
            }
            inputStream!!.close()
        } catch (e: FileNotFoundException) {

        } catch (e: IOException) {

        }

        return bitmap

    }

    fun combineImages(c: Bitmap, s: Bitmap): Bitmap { // can add a 3rd parameter
        // 'String loc' if you
        // want to save the new
        // image - left some
        // code to do that at
        // the bottom
        var cs: Bitmap? = null

        val width: Int
        var height = 0

        if (c.width > s.width) {
            width = c.width
            height = c.height + s.height
        } else {
            width = s.width
            height = c.height + s.height
        }

        cs = Bitmap.createBitmap(width, height, Config.ARGB_8888)

        val comboImage = Canvas(cs!!)

        comboImage.drawBitmap(c, 0f, 0f, null)
        comboImage.drawBitmap(s, 0f, c.height.toFloat(), null)

        return cs

    }

    /**
     * Gets the com.gsoft.pathfindervs.network type.
     *
     * @param ctx the ctx
     * @return the com.gsoft.pathfindervs.network type
     */
    fun getNetworkType(ctx: Context): String {

        val cm = ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // int networkType = cm.getActiveNetworkInfo().getType();
        if (cm != null) {
            val networkTypeName = cm.activeNetworkInfo
                    .typeName

            val tm = ctx
                    .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val telNetworkType = tm.networkType

            return networkTypeName + " - " + telNetworkType
        }
        return ""
    }

    companion object {

        internal val TAG = "AppUtil"

        /**
         * The Constant SECOND.
         */
        val SECOND: Long = 1000

        /**
         * The Constant MINT.
         */
        val MINT = 60 * AppUtil.SECOND

        /**
         * The Constant HOUR.
         */
        val HOUR = 60 * AppUtil.MINT

        /**
         * The Constant DAY.
         */
        val DAY = 24 * AppUtil.HOUR

        val WEEK = 7 * AppUtil.DAY

        val MONTH = 30 * AppUtil.DAY

        val YEAR = 12 * AppUtil.MONTH

        // url + append token

        fun isNetworkAvailable(context: Context): Boolean {
            val manager = context.getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
            //For 3G check
            val is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting
            //For WiFi Check
            val isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting
            //        System.out.println(is3g + " net " + isWifi);
            return if (!is3g && !isWifi) {
                false
            } else {
                true
            }
        }

        // Printing HashKey
        fun KeyHash(context: Context, packageName: String) {

            try {
                val info = context.packageManager.getPackageInfo(
                        packageName, PackageManager.GET_SIGNATURES)
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    Log.d("Followit",
                            Base64.encodeToString(md.digest(), Base64.DEFAULT))
                }
            } catch (e: NameNotFoundException) {
            } catch (e: NoSuchAlgorithmException) {
            }

        }

        fun callIntent(c: Context, cls: Class<*>) {
            val intent = Intent(c, cls)
            c.startActivity(intent)
        }

        fun showAlertDialog(context: Context, title: String, msg: String) {
            val alertDialog = AlertDialog.Builder(context).create()

            // Setting Dialog Title
            alertDialog.setTitle(title)

            // Setting Dialog Message
            alertDialog.setMessage(msg)

            // Setting Icon to Dialog
            // alertDialog.setIcon(R.drawable.tick);

            // Setting OK Button
            alertDialog.setButton("OK") { dialog, which -> }
            alertDialog.show()
        }

        fun getHeaders(context: Context?): Map<String, String> {
            val temp: MutableMap<String, String> = HashMap<String, String>();
            temp["x-access-token"] = AppPreference.getValue(context, AppWebServices.KEY_USER_ACCESS_TOKEN)
            return temp;
        }

        /**
         * Gets the app build date.
         *
         * @param context the context
         * @return the app build date
         */

        fun getAppBuildDate(context: Context): String? {

            var date: String? = null
            try {
                val ai = context.packageManager
                        .getApplicationInfo(context.packageName, 0)
                val zf = ZipFile(ai.sourceDir)
                val ze = zf.getEntry("classes.dex")
                val time = ze.time
                date = AppUtil.convertDateToString(Date(time))

            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return date
        }

        /**
         * Convert date to string.
         *
         * @param sDate the s date
         * @return the string
         */
        fun convertDateToString(sDate: Date): String {

            val sdf = SimpleDateFormat("EEE, d MMM")
            return sdf.format(sDate)
        }


        /**
         * Gets the current time.
         *
         * @return the current time
         */
        val currentTime: String
            get() = "" + System.currentTimeMillis()

        fun decodeAdjustedFileHD(fileName: String?): Bitmap? {

            if (fileName != null) {
                val imageFile = File(fileName)
                return adjustOrientation(decodeFile(imageFile, 400),
                        imageFile.toURI())
            }

            return null
        }

        fun saveHDFile(imageSourcePath: String): String {
            try {

                //            Bitmap bmp = AppUtil.decodeAdjustedFileHD(imageSourcePath);
                val bmp = UtilityImage.adjustImageOrientation(imageSourcePath)

                val imageDestinationPath = AppFolders.getRandomImageFilePath()//getRandomeFileName().getAbsolutePath();
                val path = AppFolders.getImageFilePath()
                if (!path.exists()) {
                    path.mkdirs()
                }
                val file = File(imageDestinationPath)
                if (!file.exists()) {
                    file.createNewFile()
                }
                var fos: FileOutputStream? = null

                fos = FileOutputStream(file)
                if (fos != null) {
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                    fos.close()
                }
                // rotatedBitmap.recycle();
                bmp.recycle()
                return imageDestinationPath
            } catch (exception: IOException) {
                DebugHelper.print("Image", exception, true)
            } catch (exception: NullPointerException) {
                DebugHelper.print("Image", exception, true)
            }

            return imageSourcePath
        }

        fun adjustOrientation(bmp: Bitmap?, uri: URI): Bitmap? {
            try {
                val exif = ExifInterface(uri.path)
                val orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL)
                val matrix = Matrix()
                if (orientation == 6) {
                    matrix.postRotate(90f)
                }
                return Bitmap.createBitmap(bmp!!, 0, 0, bmp.width,
                        bmp.height, matrix, true)

            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        fun decodeFile(f: File, simpleSize: Int): Bitmap? {

            try {
                // decode image size
                val o = Options()
                o.inJustDecodeBounds = true
                BitmapFactory.decodeStream(FileInputStream(f), null, o)

                // Find the correct scale value. It should be the power of 2.
                var width_tmp = o.outWidth
                var height_tmp = o.outHeight
                var scale = 1
                while (true) {
                    if (width_tmp / 2 < simpleSize || height_tmp / 2 < simpleSize) {
                        break
                    }
                    width_tmp /= 2
                    height_tmp /= 2
                    scale *= 2
                }

                // decode with inSampleSize
                val o2 = Options()
                o2.inSampleSize = scale
                return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
            } catch (exception: FileNotFoundException) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            } catch (error: OutOfMemoryError) {
                DebugHelper.trackError(error)
            }

            return null
        }

        fun decodeFileHD(fileName: String?): Bitmap? {

            if (fileName != null) {
                val imageFile = File(fileName)
                return decodeFile(imageFile, 200)
            }

            return null
        }


        fun getDecoratedHint(et: EditText, hintText: CharSequence, icon: Drawable): CharSequence {
            val ssb = SpannableStringBuilder("   ")
            ssb.append(hintText)
            val textSize = (et.textSize * 1.25).toInt()
            icon.setBounds(0, 0, textSize, textSize)
            ssb.setSpan(ImageSpan(icon), 1, 2,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            return ssb
        }


        /**
         * Change time from 24 hour format to 12 hour format
         *
         * @param string time
         */
        fun convertTimeTo12hourFormat(time: String): String {
            val f1 = SimpleDateFormat("HH:mm:ss") // HH for hour of the
            // day (0 - 23)
            var d: Date? = null
            try {
                d = f1.parse(time)
            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            val f2 = SimpleDateFormat("h:mma")
            return f2.format(d).toLowerCase()
        }

        fun convertDateTimeTo12hourFormat(dateTime: String): String {

            var s = "00:00:00"
            try {
                if (dateTime.contains("T")) {
                    val result = dateTime.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    s = result[1]
                } else {
                    s = dateTime
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            val f1 = SimpleDateFormat("HH:mm:ss") // HH for hour of the
            // day (0 - 23)
            var d: Date? = null
            try {
                d = f1.parse(s)
            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            val f2 = SimpleDateFormat("h:mma")
            return f2.format(d).toLowerCase()
        }

        fun convertDateTimeToDateWithSlashes(dateTime: String): String {

            var s = "00:00:00"
            try {
                if (dateTime.contains("T")) {
                    val result = dateTime.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    s = result[0]
                } else {
                    s = dateTime
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            val f1 = SimpleDateFormat("yyyy-MM-dd") // HH for hour of
            // the
            // day (0 - 23)
            var d: Date? = null
            try {
                d = f1.parse(s)
            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            val f2 = SimpleDateFormat("dd/MM/yyyy")
            return f2.format(d).toLowerCase()
        }

        fun splitFullDateTimeFormat(dateTime: String): String {
            var s = ""
            try {
                if (dateTime.contains("T")) {
                    val result = dateTime.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    s = result[0]
                    return s
                } else {
                    s = dateTime
                    return dateTime
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return s
        }

        fun overlay(bmp1: Bitmap, bmp2: Bitmap): Bitmap {
            val bmOverlay = Bitmap.createBitmap(bmp1.width,
                    bmp1.height, bmp1.config)
            val canvas = Canvas(bmOverlay)
            canvas.drawBitmap(bmp1, Matrix(), null)
            canvas.drawBitmap(bmp2, 0f, 0f, null)
            return bmOverlay
        }

        /**
         * Save image.
         *
         * @param bitmap   the bitmap
         * @param path     the path
         * @param fileName the file name
         * @param context  the context
         * @return the string
         */
        fun saveImage(bitmap: Bitmap, path: String, fileName: String,
                      context: Context): String? {

            return AppUtil.saveImage(bitmap, path, fileName, context, false)
        }

        /**
         * Save image.
         *
         * @param bitmap   : its the bitmap data to be written to the file
         * @param path     : should start and end with a slash e.g. /path/
         * @param fileName : should be a string with extension without any slashes e.g.
         * fileName.ext
         * @param context  : context is cascading from the action-starting-activity to
         * this function
         * @param replace  the recycle
         * @return the string
         * @returns fileName: file on which the file was written
         */
        fun saveImage(bitmap: Bitmap?, path: String, fileName: String,
                      context: Context, replace: Boolean): String? {

            var outStream: OutputStream? = null

            var file: File? = null
            try {
                file = File(path, "")
                file.mkdirs()
                val imgFile = File(path + fileName)
                if (File(path + fileName).exists() == true) {
                    imgFile.delete()
                }
                if (bitmap == null) {
                    return null
                }

                file = File(path, fileName)
                outStream = FileOutputStream(file)

                if (fileName.endsWith(".png") || fileName.endsWith(".PNG")) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
                } else if (fileName.endsWith(".jpg") || fileName.endsWith(".JPG")) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outStream)
                } else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outStream)
                }

                outStream.flush()
                outStream.close()
            } catch (exception: FileNotFoundException) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return file!!.toString()
        }

        /**
         * Does file exist.
         *
         * @param filePath the file path
         * @return true, if successful
         */
        fun doesFileExist(filePath: String): Boolean {

            var rootDir: String? = null
            var input: InputStream? = null
            try {
                rootDir = Environment.getExternalStorageDirectory().toString()
                input = FileInputStream(rootDir + filePath)

                val data = input.read()
                while (data != -1) {
                    input.close()
                    return true
                }
                input.close()
            } catch (exception: FileNotFoundException) {
                DebugHelper.trackException(AppUtil.TAG, exception)
                return false
            } catch (exception: IOException) {
                DebugHelper.trackException(AppUtil.TAG, exception)
                return false
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
                return false
            }

            input = null
            return false
        }

        fun formatPrice(price: Double): String {

            try {
                val df = DecimalFormat("$###0.00")
                return df.format(price)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return "" + price
        }

        /**
         * Gets the rounded corner bitmap.
         *
         * @param bitmap  the bitmap
         * @param pixels  the pixels
         * @param context the context
         * @return the rounded corner bitmap
         */
        fun getRoundedCornerBitmap(bitmap: Bitmap?, pixels: Int,
                                   context: Context): Bitmap? {

            var output: Bitmap? = null
            try {
                if (bitmap == null || bitmap.isRecycled) {
                    return null
                }
                output = Bitmap.createBitmap(bitmap.width, bitmap.height,
                        Config.ARGB_8888)
                val canvas = Canvas(output!!)

                val color = -0xbdbdbe
                val paint = Paint()

                val rect = Rect(0, 0, bitmap.width,
                        bitmap.height)
                val rectF = RectF(rect)
                val roundPx = pixels.toFloat()

                paint.isAntiAlias = true
                canvas.drawARGB(0, 0, 0, 0)
                // paint.setAlpha(0);
                paint.color = color
                canvas.drawRoundRect(rectF, roundPx, roundPx, paint)

                paint.xfermode = PorterDuffXfermode(Mode.SRC_IN)
                // paint.setAlpha(255);
                canvas.drawBitmap(bitmap, rect, rect, paint)
                if (output == null || output.isRecycled) {
                    return bitmap
                }
                if (bitmap != null && bitmap.isRecycled == false) {
                    bitmap.recycle()
                }
                return output
            } catch (e: OutOfMemoryError) {
                DebugHelper.trackError(AppUtil.TAG, e)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            if (output != null && output.isRecycled == false) {
                output.recycle()
            }
            return bitmap
        }

        // decodes image and scales it to reduce memory consumption

        /**
         * Decode file.
         *
         * @param f the f
         * @return the bitmap
         */
        fun decodeFile(f: File): Bitmap? {

            try {
                // decode image size
                val o = Options()
                o.inJustDecodeBounds = true
                BitmapFactory.decodeStream(FileInputStream(f), null, o)

                // Find the correct scale value. It should be the power of 2.
                val REQUIRED_SIZE = 100
                var width_tmp = o.outWidth
                var height_tmp = o.outHeight
                var scale = 1
                while (true) {
                    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                        break
                    }
                    width_tmp /= 2
                    height_tmp /= 2
                    scale *= 2
                }

                // decode with inSampleSize
                val o2 = Options()
                o2.inSampleSize = scale
                return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
            } catch (exception: FileNotFoundException) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return null
        }

        /**
         * Checks if is saved.
         *
         * @param filePath the file path
         * @return true, if is saved
         */
        fun isSaved(filePath: String): Boolean {

            var isFileSaved = false
            try {
                val file = File(filePath)
                isFileSaved = file.exists()
            } catch (exception: Exception) {

                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return isFileSaved
        }

        /**
         * Delete hidden folder.
         *
         * @return true, if successful
         */
        fun deleteHiddenFolder(): Boolean {
            var isDeleted = false
            try {
                // FileUtils
                // .deleteDirectory(new File(AppFolders.getHiddenFolderPath()));
                isDeleted = true
            } catch (exception: Exception) {
                DebugHelper.printException(TAG, exception)
            }

            return isDeleted
        }

        /**
         * Removes the directory.
         *
         * @param directory the directory
         * @return true, if successful
         */
        fun removeDirectory(directory: String?): Boolean {

            return if (directory != null && directory.length > 0) {
                AppUtil.removeDirectory(File(directory))
            } else false
        }

        /**
         * Removes the directory.
         *
         * @param directory the directory
         * @return true, if successful
         */
        fun removeDirectory(directory: File?): Boolean {

            if (directory == null) {
                return false
            }
            if (!directory.exists()) {
                return true
            }
            if (directory.isDirectory) {
                val list = directory.list()
                // Some JVMs return null for File.list() when the
                // directory is empty.
                if (list != null) {
                    for (element in list) {
                        val entry = File(directory, element)
                        if (entry.isDirectory) {
                            if (!AppUtil.removeDirectory(entry)) {
                                return false
                            }
                        } else {
                            if (!entry.delete()) {
                                return false
                            }
                        }
                    }
                }
            }
            return directory.delete()
        }

        /**
         * Gets the directory files list.
         *
         * @param directory the directory
         * @return the directory files list
         */
        fun getDirectoryFilesList(directory: String): HashMap<String, File> {

            return AppUtil.getDirectoryFilesList(File(directory))
        }

        /**
         * Gets the directory files list.
         *
         * @param directory the directory
         * @return the directory files list
         */
        fun getDirectoryFilesList(directory: File?): HashMap<String, File> {

            val directoryList = HashMap<String, File>()

            if (directory == null) {
                return directoryList
            }
            if (!directory.exists()) {
                return directoryList
            }
            if (directory.isDirectory) {
                val list = directory.list()
                // Some JVMs return null for File.list() when the
                // directory is empty.
                if (list != null) {
                    for (element in list) {
                        val entry = File(directory, element)
                        if (!entry.isDirectory) {
                            var name = entry.name
                            if (name.length > 0 && name.contains(".")) {
                                name = name.subSequence(0,
                                        name.lastIndexOf('.')) as String
                                directoryList.put(name, entry)
                            }
                        }
                    }
                }
            }
            return directoryList
        }

        /**
         * Show toast.
         *
         * @param appContext the app context
         * @param message    the message
         */
        fun showToast(appContext: Context, message: String) {

            Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show()
        }

        fun showToastLong(appContext: Context, message: String) {

            Toast.makeText(appContext, message, Toast.LENGTH_LONG).show()
        }

        // public static void playAudioVideo(final LCVideo LCVideo,
        // final boolean isWatchInstant, final Context activity) {
        // if (LCVideo.getFileFolderName() == LtdFolders.AUDIO) {
        // LTDPlayerFactory.startLTDAudioPlayer(LCVideo, activity);
        // return;
        // }
        // String localPath = null;
        // File file = null;
        // try {
        //
        // if (isWatchInstant == false) {
        //
        // if (LCVideo.isCanEncrypt()) {
        // localPath = LCVideo.getHiddenFilePath();
        // } else {
        // localPath = LCVideo.getFilePath();
        // }
        //
        // file = new File(localPath);
        //
        // } else {
        //
        // localPath = SMUtility.createFileUrl(
        // LCVideo.getFileNameServer(), activity,
        // LCVideo).toString();
        // localPath = localPath.replace("https", "http");
        //
        // }
        //
        // final Intent intent = new Intent();
        // intent.setAction(android.content.Intent.ACTION_VIEW);
        //
        // if (isWatchInstant == true) {
        //
        // final String tempLocalPath = new String(localPath);
        //
        // BGWorkerDefiner workToDone = new BGWorkerDefiner() {
        //
        // @Override
        // public Object performInBackground() {
        // return tempLocalPath;
        // // return getShortURL(tempLocalPath);
        // }
        //
        // @Override
        // public void finalResult() {
        // }
        //
        // @Override
        // public void callback(Object result) {
        //
        // String shortPath = (String) result;
        // String finalPath = new String(tempLocalPath);
        // if (shortPath != null && shortPath.length() > 0) {
        // finalPath = shortPath;
        // }
        // intent.setDataAndType(Uri.parse(finalPath), "video/*");
        // LTDMediaApplication.G_TRACKER
        // .trackLiveStreamingEvent(LCVideo);
        // executeIntentForplayVideo(intent, finalPath,
        // LCVideo, activity);
        // }
        // };
        //
        // new BGWorkerExecuter(workToDone, activity, "Processing...",
        // true).execute();
        //
        // } else {
        // intent.setDataAndType(Uri.fromFile(file), "video/*");
        // executeIntentForplayVideo(intent, localPath, LCVideo,
        // activity);
        // }
        // } catch (Exception exception) {
        // DQDebugHelper.printAndTrackException(TAG , exception);
        // }
        // }
        //
        // // We are using this method to exectue only video intents. Audio intent
        // will
        // // be handled in LTDPlayerFactory
        // private static void executeIntentForplayVideo(Intent intent,
        // String localPath, LCVideo LCVideo, final Context context) {
        // try {
        // List<ResolveInfo> intents = context.getPackageManager()
        // .queryIntentActivities(intent,
        // PackageManager.MATCH_DEFAULT_ONLY);
        //
        // if (intents != null && intents.size() > 0) {
        // context.startActivity(intent);
        // } else {
        //
        // Intent videoIntent = new Intent(context,
        // CommonVideoPlayerActivity.class);
        // videoIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
        // | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        // videoIntent.putExtra("localPath", localPath);
        // videoIntent.putExtra("title", LCVideo.getTitle());
        // CommonVideoPlayerActivity.LCVideo = LCVideo;
        // context.startActivity(videoIntent);
        // }
        // } catch (Exception exception) {
        // DQDebugHelper.printAndTrackException(TAG , exception);
        // }
        // }
        //
        // public static String[] getLTDIds(Context context) {
        // String[] ltdIdList = null;
        // SMUtility smUtility = SMUtility.getInstance(context);
        // SharedPreferences pref = smUtility.getSharedPreferences();
        // String ltdIds = pref.getString(SMConstants.LIST_OF_USER_NAMES, null);
        // if (ltdIds != null) {
        // ltdIdList = ltdIds.split(",");
        // }
        // return ltdIdList;
        //
        // }
        //
        // public static void saveLTDIdToIDList(Context context, String userName) {
        // String[] ltdIdList = null;
        // SMUtility smUtility = SMUtility.getInstance(context);
        // SharedPreferences pref = smUtility.getSharedPreferences();
        // String ltdIds = pref.getString(SMConstants.LIST_OF_USER_NAMES, "");
        // if (ltdIds != null) {
        // ltdIdList = ltdIds.split(",");
        // boolean flag = false;
        // for (String string : ltdIdList) {
        // if (string.equalsIgnoreCase(userName) == true) {
        // flag = true;
        // break;
        // }
        // }
        // if (flag == false || ltdIds == null) {
        // if (ltdIds == null || ltdIds.length() <= 0) {
        // smUtility.savePerefference(SMConstants.LIST_OF_USER_NAMES,
        // userName);
        // } else {
        // smUtility.savePerefference(SMConstants.LIST_OF_USER_NAMES,
        // ltdIds + "," + userName);
        // }
        // }
        // }
        //
        // }

        /**
         * Format size.
         *
         * @param expectedBytes the expected bytes
         * @return the string
         */
        fun formateSize(expectedBytes: Long): String? {

            var result: String? = null
            if (expectedBytes < 1024) {
                result = String.format("%d B", expectedBytes)
            } else if (expectedBytes < 1024 * 1024) {
                val value = expectedBytes.toFloat() / 1024
                result = String.format("%1.2f KB", value)
            } else if (expectedBytes < 1024 * 1024 * 1024) {
                val value = expectedBytes.toFloat() / (1024 * 1024)
                result = String.format("%1.2f MB", value)
            } else {
                val value = expectedBytes.toFloat() / (1024 * 1024 * 1024)
                result = String.format("%1.2f GB", value)
            }
            return result
        }

        /**
         * Formate size.
         *
         * @param expectedBytes the expected bytes
         * @return the string
         */
        fun formateSizeInMB(expectedBytes: Long): String? {

            var result: String? = null
            if (expectedBytes >= 1024 * 1024 * 1024) {
                val value = expectedBytes.toFloat() / (1024 * 1024 * 1024)
                result = String.format("%1.2f GB", value)

            } else {
                val value = expectedBytes.toFloat() / (1024 * 1024)
                result = String.format("%1.2f MB", value)
            }
            return result
        }

        /**
         * Sets the list view height based on children.
         *
         * @param listView the new list view height based on children
         */
        fun setListViewHeightBasedOnChildren(listView: ListView) {

            val listAdapter = listView.adapter ?: // pre-condition
            return

            var totalHeight = 0
            for (i in 0 until listAdapter.count) {
                val listItem = listAdapter.getView(i, null, listView)
                (listItem as? ViewGroup)?.layoutParams = LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
                listItem.measure(0, 0)
                totalHeight += listItem.measuredHeight
            }

            val params = listView.layoutParams
            params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
            listView.layoutParams = params
        }

        /**
         * Check for updates message.
         *
         * @param context the context
         */
        fun checkForUpdatesMessage(context: Context) {

            // try {
            //
            // long updateType =
            // sharedPreferences.getLong(VersionUpgrade.UPDATE_AVAILABLE, -1);
            // final String urlLTDMedia =
            // sharedPreferences.getString(VersionUpgrade.UPDATE_URL, "");
            //
            // if (updateType == VersionUpgradeActivity.FORCE_UPDATE) {
            // Intent intent = new Intent(context, VersionUpgradeActivity.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // context.startActivity(intent);
            // } else if (updateType == VersionUpgradeActivity.NORMAL_UPDATE) {
            // if (context != null) {
            //
            // AlertDialog.Builder builder = new AlertDialog.Builder(context);
            // builder.setMessage(context.getString(R.string.msg_normal_updates_are_available))
            // .setCancelable(false)
            // .setPositiveButton("Update", new DialogInterface.OnClickListener() {
            // public void onClick(DialogInterface dialog, int id) {
            // dialog.cancel();
            // dialog.dismiss();
            // Intent intent = new Intent(Intent.ACTION_VIEW);
            // intent.setData(Uri.parse(urlLTDMedia));
            // context.startActivity(intent);
            //
            // }
            // }).setNegativeButton("Ignore", new DialogInterface.OnClickListener()
            // {
            // public void onClick(DialogInterface dialog, int id) {
            // /*
            // * Ignore normal updates for three days.
            // */
            // long threeDaysTime = 3 * 24 * 60 * 60 * 1000;
            // long currentTime = System.currentTimeMillis();
            // long updateIgnoreTime = currentTime + threeDaysTime;
            // savePerefference(VersionUpgrade.UPDATE_IGNORE_TIME,
            // updateIgnoreTime);
            // dialog.cancel();
            // }
            // });
            //
            // AlertDialog alert = builder.create();
            // alert.show();
            //
            // }
            // } else if (updateType ==
            // VersionUpgradeActivity.NETWORK_NOT_AVAILABLE) {
            // if (context != null) {
            //
            // AlertDialog.Builder builder = new AlertDialog.Builder(context);
            // builder.setMessage(context.getString(R.string.error_msg_network_not_reachable))
            // .setCancelable(false)
            // .setPositiveButton(context.getString(R.string.btn_try_again),
            // new DialogInterface.OnClickListener() {
            // public void onClick(DialogInterface dialog, int id) {
            // dialog.cancel();
            // dialog.dismiss();
            // AsyncVersionUpgradeTask asyncVersionUpgradeTask = new
            // AsyncVersionUpgradeTask(
            // context);
            // asyncVersionUpgradeTask.execute();
            //
            // }
            // })
            // .setNegativeButton(context.getString(R.string.btn_nevermind),
            // new DialogInterface.OnClickListener() {
            // public void onClick(DialogInterface dialog, int id) {
            // dialog.cancel();
            // dialog.dismiss();
            // }
            // });
            //
            // AlertDialog alert = builder.create();
            // alert.show();
            //
            // }
            // } else {
            // if (context != null) {
            //
            // AlertDialog.Builder builder = new AlertDialog.Builder(context);
            // builder.setMessage(context.getString(R.string.msg_no_updates_available))
            // .setCancelable(false)
            // .setPositiveButton(context.getString(R.string.btn_ok),
            // new DialogInterface.OnClickListener() {
            // public void onClick(DialogInterface dialog, int id) {
            // dialog.cancel();
            // dialog.dismiss();
            // }
            // });
            //
            // AlertDialog alert = builder.create();
            // alert.show();
            //
            // }
            // }
            // } catch (Exception exception) {
            // DQDebugHelper.printAndTrackException(TAG , exception);
            // }

        }

        /**
         * Conver string to date.
         *
         * @param stringDate the string date
         * @return the date
         */
        fun converStringToDate(stringDate: String?): Date? {
            if (stringDate == null) {
                return null
            }

            try {
                val dateFormat = SimpleDateFormat(
                        "yyyy-MM-dd'T'HH:mm:ss")// ("yyyy-mm-ddTHH:mm:ss zzzz"");
                return dateFormat.parse(stringDate.substring(0, 18))
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return null
        }

        fun getMonthNameAndDay(mDateString: String): String {
            val dateFormat = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss")// ("yyyy-mm-ddTHH:mm:ss zzzz"");
            var date: Date? = null
            try {
                date = dateFormat.parse(mDateString)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return ("" + SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)
                    + ", " + date!!.date + "/" + (date.month + 1))
        }

        /**
         * Convert date to simple string.
         *
         * @param sDate the s date
         * @return the string
         */
        fun convertDateToSimpleString(sDate: Date): String? {

            try {
                val dateFormat = SimpleDateFormat(
                        "MMM dd, yyyy")
                return dateFormat.format(sDate)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return null
        }

        /**
         * Gets the full time estimations.
         *
         * @param statTime the stat time
         * @return the full time estimations
         */
        fun getFullTimeEstimations(statTime: Long): String {

            var timeRemainingStr = ""
            try {

                val timeRemaining = System.currentTimeMillis() - statTime
                if (timeRemaining < AppUtil.DAY) {
                    var time = timeRemaining
                    val hours = (timeRemaining / AppUtil.HOUR).toInt()
                    time = timeRemaining - hours * AppUtil.HOUR
                    val minutes = (time / AppUtil.MINT).toInt()
                    timeRemainingStr = String.format("%d hours ago", timeRemaining / AppUtil.HOUR, minutes)
                } else {
                    var time = timeRemaining
                    val days = (timeRemaining / AppUtil.DAY).toInt()
                    time = timeRemaining - days * AppUtil.DAY
                    val hours = (time / AppUtil.HOUR).toInt()
                    time = time - hours * AppUtil.HOUR
                    val mints = (time / AppUtil.MINT).toInt()
                    timeRemainingStr = String.format("%d Days, %d hours ago", days,
                            hours, mints)
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return timeRemainingStr
        }

        fun getHoursEstimations(mPostTime: Long): String {
            val format = SimpleDateFormat("h")
            var strhours = ""
            try {
                val timeRemaining = System.currentTimeMillis() - mPostTime
                strhours = format.format(timeRemaining)
                val fhours = java.lang.Long.parseLong(strhours)
                if (fhours > 1) {
                    strhours = strhours + " hours"
                } else {
                    strhours = strhours + " hour"
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return strhours
        }

        /**
         * Gets the theater time estimations.
         *
         * @param date the date
         * @return the theater time estimations
         */
        fun getTheaterTimeEstimations(date: Date): String? {

            val statTime = date.time
            var timeRemainingStr: String? = ""
            try {

                val timeRemaining = System.currentTimeMillis() - statTime

                if (timeRemaining <= AppUtil.MINT * 10) {
                    timeRemainingStr = "Just now"
                } else if (timeRemaining <= AppUtil.HOUR) {
                    val mints = (timeRemaining / AppUtil.MINT).toInt()
                    timeRemainingStr = String.format("%d minutes ago", mints)
                } else if (timeRemaining <= AppUtil.HOUR * 2) {
                    timeRemainingStr = String.format("%d hour ago", 1)
                } else if (timeRemaining <= AppUtil.HOUR * 24) {
                    val hours = (timeRemaining / AppUtil.HOUR).toInt()
                    timeRemainingStr = String.format("%d hours ago", hours)
                } else if (timeRemaining <= AppUtil.DAY * 2) {
                    timeRemainingStr = String.format("%d day ago", 1)
                } else if (timeRemaining <= AppUtil.DAY * 30) {
                    val days = (timeRemaining / AppUtil.DAY).toInt()
                    timeRemainingStr = String.format("%d days ago", days)
                } else {
                    timeRemainingStr = AppUtil.convertDateToSimpleString(date)
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return timeRemainingStr
        }

        /**
         * Format time.
         *
         * @param mills the mills
         * @return the string
         */
        fun formatTime(mills: Long): String {

            try {

                val seconds = (mills / 1000).toInt() % 60
                val minutes = (mills / (1000 * 60) % 60).toInt()
                val hours = (mills / (1000 * 60 * 60) % 24).toInt()

                return String.format("%02d:%02d:%02d", hours, minutes, seconds)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return "00:00:00"
        }

        /**
         * Creates the user fb imag url.
         *
         * @param userId the user id
         * @return the string
         */
        fun createUserFBImagUrl(userId: String): String {

            return ("http://graph.facebook.com/" + userId
                    + "/picture?width=210&height=240")
        }

        /**
         * Update user fonts.
         *
         * @param context the context
         * @param root    the root
         */
        fun updateUserFonts(context: Context, root: ViewGroup) {

            try {
                val mFont = Typeface.createFromAsset(
                        context.assets, "fonts/18836_HELR45W.ttf")
                AppUtil.setFont(context, root, mFont)
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

        }

        /**
         * Update user fonts.
         *
         * @param context the context
         * @param root    the root
         */
        fun updateUserFonts(context: Context, root: View) {

            try {
                var mFont = Typeface.createFromAsset(context.assets,
                        "fonts/18836_HELR45W.ttf")
                if (root is ViewGroup) {
                    AppUtil.setFont(context, root, mFont)
                } else if (root is TextView) {
                    if (root.typeface != null && root.typeface.isBold) {
                        mFont = Typeface.createFromAsset(context.assets,
                                "fonts/18923_HelveticaNeueHv.ttf")
                        root.typeface = mFont
                    } else {
                        mFont = Typeface.createFromAsset(context.assets,
                                "fonts/18836_HELR45W.ttf")
                        root.typeface = mFont
                    }
                } else if (root is Button) {
                    if (root.typeface != null && root.typeface.isBold) {
                        mFont = Typeface.createFromAsset(context.assets,
                                "fonts/18923_HelveticaNeueHv.ttf")
                        root.typeface = mFont
                    } else {
                        mFont = Typeface.createFromAsset(context.assets,
                                "fonts/18836_HELR45W.ttf")
                        root.typeface = mFont
                    }
                } else if (root is ViewGroup) {
                    AppUtil.setFont(context, root, mFont)
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

        }

        /**
         * Sets the font.
         *
         * @param context the context
         * @param group   the group
         * @param font    the font
         */
        fun setFont(context: Context, group: ViewGroup, font: Typeface) {
            var font = font

            try {
                val count = group.childCount
                var v: View
                for (i in 0 until count) {
                    v = group.getChildAt(i)
                    if (v is TextView) {
                        val curView = v
                        if (curView.typeface != null && curView.typeface.isBold) {
                            font = Typeface.createFromAsset(context.assets,
                                    "fonts/18923_HelveticaNeueHv.ttf")
                            curView.typeface = font
                        } else {
                            font = Typeface.createFromAsset(context.assets,
                                    "fonts/18836_HELR45W.ttf")
                            curView.typeface = font
                        }
                    } else if (v is Button) {
                        val curView = v
                        if (curView.typeface != null && curView.typeface.isBold) {
                            font = Typeface.createFromAsset(context.assets,
                                    "fonts/18923_HelveticaNeueHv.ttf")
                            curView.typeface = font
                        } else {
                            font = Typeface.createFromAsset(context.assets,
                                    "fonts/18836_HELR45W.ttf")
                            curView.typeface = font
                        }
                    } else if (v is ViewGroup) {
                        AppUtil.setFont(context, v, font)
                    }
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

        }

        /**
         * Creates the user fb id.
         *
         * @param uid the uid
         * @return the string
         */
        fun createUserFBId(uid: String): String {

            return "" + uid + ".jpg"
        }

        fun updateButtonSatate(view: View?, enable: Boolean) {
            if (view != null) {
                view.isClickable = enable
            }
        }

        fun toInitCap(param: String?): String {

            if (param != null && param.length > 0) {
                val charArray = param.toCharArray() // convert into char array
                charArray[0] = Character.toUpperCase(charArray[0]) // set capital
                // letter to
                // first
                // postion
                return String(charArray) // return desired output
            } else {
                return ""
            }
        }

        /**
         * Format milli seconds to time.
         *
         * @param milliseconds the milliseconds
         * @return the string
         */
        fun formatMilliSecondsToTime(milliseconds: String): String {

            try {
                val mills = java.lang.Long.parseLong(milliseconds)

                val seconds = (mills / 1000).toInt() % 60
                val minutes = (mills / (1000 * 60) % 60).toInt()
                val hours = (mills / (1000 * 60 * 60) % 24).toInt()

                return (String.format("%02d", hours) + ":"
                        + String.format("%02d", minutes) + ":"
                        + String.format("%02d", seconds))
            } catch (exception: Exception) {
                DebugHelper.trackException(TAG, exception)
            }

            return "00:00:00"
        }

        fun formatLongToTime(mills: Long): String {

            try {

                val seconds = (mills / 1000).toDouble()
                val minutes = (mills / (1000 * 60) % 60).toInt()
                val hours = (mills / (1000 * 60 * 60) % 24).toInt()
                val builder = StringBuilder()
                if (hours > 0) {
                    builder.append(String.format("%.2d Hour ", hours))
                }
                if (minutes > 0) {
                    builder.append(String.format("%.2d Mints ", minutes))
                }

                if (seconds > 0) {
                    builder.append(String.format("%.2f Seconds ", seconds))
                }

                return builder.toString()
            } catch (exception: Exception) {
                DebugHelper.trackException(TAG, exception)
            }

            return ""
        }

        /**
         * Format milli seconds to date.
         *
         * @param milliseconds the milliseconds
         * @param dateFormat   the date format
         * @return the string
         */
        fun formatMilliSecondsToDate(milliseconds: String,
                                     dateFormat: String): String {

            var df: DateFormat = SimpleDateFormat(dateFormat)
            try {
                val mills = java.lang.Long.parseLong(milliseconds)
                val d = Date(mills * 1000)
                df = SimpleDateFormat(dateFormat)
                df.setTimeZone(TimeZone.getTimeZone("UTC"))
                return df.format(d)
            } catch (exception: Exception) {
                DebugHelper.trackException(TAG, exception)
            }

            val d = Date()
            return df.format(d)
        }

        /**
         * Join.
         *
         * @param array      the array
         * @param separator  the separator
         * @param startIndex the start index
         * @param endIndex   the end index
         * @return the string
         */
        fun join(array: Array<Any>?, separator: String?,
                 startIndex: Int, endIndex: Int): String? {
            var separator = separator

            if (array == null) {
                return null
            }
            if (separator == null) {
                separator = ""
            }
            var bufSize = endIndex - startIndex
            if (bufSize <= 0) {
                return ""
            }

            bufSize *= (if (array[startIndex] == null)
                16
            else
                array[startIndex]
                        .toString().length) + separator.length

            val buf = StringBuffer(bufSize)

            for (i in startIndex until endIndex) {
                if (i > startIndex) {
                    buf.append(separator)
                }
                if (array[i] != null) {
                    buf.append(array[i])
                }
            }
            return buf.toString()
        }


        /**
         * Make first word capital.
         *
         * @param sentence the sentence
         * @return the string
         */
        fun makeFirstWordCapital(sentence: String): String {

            val result = StringBuilder(sentence.length)
            val words = sentence.split("\\s".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            var i = 0
            val l = words.size
            while (i < l) {
                if (i > 0) {
                    result.append(" ")
                }
                result.append(Character.toUpperCase(words[i][0])).append(
                        words[i].substring(1))
                ++i

            }
            return result.toString()
        }

        /**
         * Human readable byte count.
         *
         * @param bytes the bytes
         * @param si    the si
         * @return the string
         */
        fun humanReadableByteCount(bytes: Long,
                                   si: Boolean): String {

            val unit = if (si) 1000 else 1024
            if (bytes < unit) {
                return bytes.toString() + " B"
            }
            val exp = (Math.log(bytes.toDouble()) / Math.log(unit.toDouble())).toInt()
            val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
            return String.format("%.1f %sB", bytes / Math.pow(unit.toDouble(), exp.toDouble()), pre)
        }

        /**
         * Format upload percentage.
         *
         * @param url           the url
         * @param uploadedBytes the uploaded bytes
         * @return the string
         */
        fun formatUploadPercentage(url: String,
                                   uploadedBytes: Long): String? {

            try {
                val f = File(url)
                val contentLength = f.length()
                val progressPercentage = uploadedBytes.toDouble() / contentLength * 100
                var p = Math.floor(progressPercentage).toInt()
                if (p > 100) {
                    p = 100
                }
                return p.toString()
            } catch (exception: Exception) {
                DebugHelper.trackException(TAG, exception)
            }

            return null
        }

        /**
         * Format string.
         *
         * @param value            the value
         * @param literalToReplace the literal to replace
         * @param arguments        the arguments
         * @return the string
         */
        fun formatString(value: String, literalToReplace: String,
                         arguments: String): String {

            return value.replace(literalToReplace.toRegex(), arguments)
        }

        fun convertInSimpleDateTime(sDate: Date?): String {

            if (sDate == null) {
                return ""
            }
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return sdf.format(sDate)
        }

        fun convertToCompletedDateTime(sDate: Date?): String {

            if (sDate == null) {
                return ""
            }
            val sdf = SimpleDateFormat("hh:mm dd:mm:yyyy")
            return sdf.format(sDate)
        }

        fun getBitmapFromView(view: View): Bitmap? {

            var returnedBitmap: Bitmap? = null
            try {

                returnedBitmap = Bitmap.createBitmap(view.width,
                        view.height, Config.ARGB_8888)
                val canvas = Canvas(returnedBitmap!!)
                val bgDrawable = view.background
                if (bgDrawable != null)
                    bgDrawable.draw(canvas)
                else
                    canvas.drawColor(Color.WHITE)
                view.draw(canvas)
            } catch (exception: Exception) {
                DebugHelper.printException(TAG, exception)
            }

            return returnedBitmap
        }

        fun isEmailValid(email: String?): Boolean {

            var isValid = false
            try {
                //String expression = "^[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$";
                val expression = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"

                val pattern = Pattern.compile(expression,
                        Pattern.CASE_INSENSITIVE)
                val matcher = pattern.matcher(email)
                if (matcher.matches()) {
                    isValid = true
                }
            } catch (e: Exception) {
                Log.w(TAG, "isEmailValid Message = " + e.toString())
                DebugHelper.printException(e)
            }

            return isValid
        }

        // public static String getTimeEstimations(long statTime) {
        //
        // String timeRemainingStr = "";
        // try {
        // long currentTime = System.currentTimeMillis() / 1000;
        // long timeRemaining = currentTime - statTime;
        // //timeRemaining = statTime;
        // if (timeRemaining < 0) {
        // timeRemainingStr = "Just Now";
        // return timeRemainingStr;
        // }
        // if (timeRemaining < AppUtil.MINT) {
        // long sec = timeRemaining / AppUtil.SECOND;
        // timeRemainingStr = String.format("%d "
        // + (sec > 1 ? "seconds" : "second"), sec);
        // } else if (timeRemaining < AppUtil.HOUR) {
        // long mint = timeRemaining / AppUtil.MINT;
        // timeRemainingStr = String.format("%d "
        // + (mint > 1 ? "minutes" : "minute"), mint);
        //
        // } else if (timeRemaining < AppUtil.DAY) {
        // long hours = timeRemaining / AppUtil.HOUR;
        // timeRemainingStr = String.format("%d "
        // + (hours > 1 ? "hours" : "hour"), hours);
        // } else if (timeRemaining < AppUtil.WEEK) {
        // long days = timeRemaining / AppUtil.DAY;
        // if (days == 1) {
        // timeRemainingStr = "Yesterday";
        // } else {
        // timeRemainingStr = String.format("%d "
        // + (days > 1 ? "days" : "day"), days);
        // }
        // } else if (timeRemaining < AppUtil.MONTH) {
        // long week = timeRemaining / AppUtil.WEEK;
        // timeRemainingStr = String.format("%d "
        // + (week > 1 ? "weeks" : "week"), week);
        // } else if (timeRemaining < AppUtil.YEAR) {
        // long months = timeRemaining / AppUtil.MONTH;
        // timeRemainingStr = String.format("%d "
        // + (months > 1 ? "months" : "month"), months);
        // } else {
        // long year = timeRemaining / AppUtil.YEAR;
        // timeRemainingStr = String.format("%d "
        // + (year > 1 ? "years" : "year"), year);
        // }
        // timeRemainingStr = timeRemainingStr + " ago";
        // } catch (final Exception exception) {
        // DebugHelper.trackException(AppUtil.TAG, exception);
        // }
        // return timeRemainingStr;
        // }

        fun getTimeEstimations(statTime: Long): String {

            var timeRemainingStr = ""
            try {
                val currentTime = System.currentTimeMillis() / 1000
                var timeRemaining = currentTime - statTime
                timeRemaining = timeRemaining * AppUtil.SECOND
                if (timeRemaining < 0) {
                    // timeRemainingStr = "Just Now";
                    timeRemainingStr = "1 mintue ago"
                    return timeRemainingStr
                }
                if (timeRemaining < AppUtil.MINT) {
                    val sec = timeRemaining / AppUtil.SECOND
                    timeRemainingStr = String.format("%d " + if (sec > 1) "seconds" else "second", sec)
                } else if (timeRemaining < AppUtil.HOUR) {
                    val mint = timeRemaining / AppUtil.MINT
                    timeRemainingStr = String.format("%d " + if (mint > 1) "minutes" else "minute", mint)

                } else if (timeRemaining < AppUtil.DAY) {
                    val hours = timeRemaining / AppUtil.HOUR
                    timeRemainingStr = String.format("%d " + if (hours > 1) "hours" else "hour", hours)
                } else if (timeRemaining < AppUtil.WEEK) {
                    val days = timeRemaining / AppUtil.DAY
                    if (days == 1L) {
                        timeRemainingStr = "Yesterday"
                    } else {
                        timeRemainingStr = String.format("%d " + if (days > 1) "days" else "day", days)
                    }
                } else if (timeRemaining < AppUtil.MONTH) {
                    val week = timeRemaining / AppUtil.WEEK
                    timeRemainingStr = String.format("%d " + if (week > 1) "weeks" else "week", week)
                } else if (timeRemaining < AppUtil.YEAR) {
                    val months = timeRemaining / AppUtil.MONTH
                    timeRemainingStr = String.format("%d " + if (months > 1) "months" else "month", months)
                } else {
                    val year = timeRemaining / AppUtil.YEAR
                    timeRemainingStr = String.format("%d " + if (year > 1) "years" else "year", year)
                }
                if (timeRemainingStr != "Yesterday") {
                    timeRemainingStr = timeRemainingStr + " ago"
                }
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return timeRemainingStr
        }

        fun getValueAppendByComma(value: Long): String {
            // TODO Auto-generated method stub
            var count = 0
            var number = "" + value
            val numberFormat = StringBuilder()
            if (number.length > 3) {
                for (i in number.length - 1 downTo 0) {
                    count++
                    numberFormat.append(number[i])
                    if (count % 3 == 0 && i > 0)
                        numberFormat.append(",")
                }
                numberFormat.reverse()
                number = numberFormat.toString()
            }
            return number
        }

        fun getValueAppendByComma(value: Double): String {
            // TODO Auto-generated method stub
            var count = 0
            var number = "" + value.toLong()
            val numberFormat = StringBuilder()
            if (number.length > 3) {
                for (i in number.length - 1 downTo 0) {
                    count++
                    numberFormat.append(number[i])
                    if (count % 3 == 0 && i > 0)
                        numberFormat.append(",")
                }
                numberFormat.reverse()
                number = numberFormat.toString()
            }
            return number
        }

        fun formatCounts(number: String): String {
            return formatCounts(java.lang.Double.parseDouble(number))
        }

        fun formatCounts(value: Double): String {
            var value = value
            val power: Int
            val suffix = " kmbt"
            var formattedNumber = ""

            val formatter = DecimalFormat("#,###.#")
            power = StrictMath.log10(value).toInt()
            value = value / Math.pow(10.0, (power / 3 * 3).toDouble())
            formattedNumber = formatter.format(value)
            formattedNumber = formattedNumber + suffix[power / 3]
            return if (formattedNumber.length > 4)
                formattedNumber.replace("\\.[0-9]+".toRegex(), "")
            else
                formattedNumber
        }

        fun getCurrentDateOrTime(milliseconds: Long): String {
            val msgDate = Date(milliseconds * 1000L)
            val msgDF = SimpleDateFormat("MMM dd")

            val currentDate = Date(System.currentTimeMillis())
            if (msgDF.format(msgDate) == msgDF.format(currentDate)) {
                val hours = SimpleDateFormat("KK:mm aa")
                return hours.format(msgDate)
            }
            return msgDF.format(msgDate)
        }

        fun getTimeStampInDate(timestamp: String): String {
            val dateTime = timestamp.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val date = dateTime[0] // 004
            val time = dateTime[1] // 034556

            val yyyyMMdd = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val yyyy = yyyyMMdd[0] // 004
            val mm = yyyyMMdd[1] // 034556
            val dd = yyyyMMdd[2] // 034556

            val hhMMss = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val hh = hhMMss[0] // 004
            val min = hhMMss[1] // 034556
            val ss = hhMMss[2] // 034556

            // Date timestampInDate = new Date(Integer.parseInt(yyyy),
            // Integer.parseInt(mm), Integer.parseInt(dd), Integer.parseInt(hh),
            // Integer.parseInt(min), Integer.parseInt(ss));
            // return getTime(timestampInDate.getTime()/10000);
            return getMonth(mm) + " " + dd
        }

        fun getTimeStampInYear(timestamp: String): String {
            val dateTime = timestamp.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val date = dateTime[0] // 004
            val time = dateTime[1] // 034556

            val yyyyMMdd = date.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val yyyy = yyyyMMdd[0] // 004
            val mm = yyyyMMdd[1] // 034556
            val dd = yyyyMMdd[2] // 034556

            val hhMMss = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val hh = hhMMss[0] // 004
            val min = hhMMss[1] // 034556
            val ss = hhMMss[2] // 034556

            // Date timestampInDate = new Date(Integer.parseInt(yyyy),
            // Integer.parseInt(mm), Integer.parseInt(dd), Integer.parseInt(hh),
            // Integer.parseInt(min), Integer.parseInt(ss));
            // return getTime(timestampInDate.getTime()/10000);
            return yyyy
        }

        fun getMonth(monthNo: String): String {
            val dfs = DateFormatSymbols()
            val months = dfs.months
            return months[Integer.parseInt(monthNo) - 1]
        }

        fun getTime(statTime: Long): String {

            var timeRemainingStr = ""
            try {
                val currentTime = System.currentTimeMillis() / 1000
                var timeRemaining = currentTime - statTime
                timeRemaining = timeRemaining * AppUtil.SECOND
                if (timeRemaining < 0) {
                    timeRemainingStr = "Just Now"
                    return timeRemainingStr
                }
                if (timeRemaining < AppUtil.MINT) {
                    val sec = timeRemaining / AppUtil.SECOND
                    timeRemainingStr = String.format("%d " + if (sec > 1) "seconds" else "second", sec)
                } else if (timeRemaining < AppUtil.HOUR) {
                    val mint = timeRemaining / AppUtil.MINT
                    timeRemainingStr = String.format("%d " + if (mint > 1) "minutes" else "minute", mint)

                } else if (timeRemaining < AppUtil.DAY) {
                    val hours = timeRemaining / AppUtil.HOUR
                    timeRemainingStr = String.format("%d " + if (hours > 1) "hours" else "hour", hours)
                } else if (timeRemaining < AppUtil.WEEK) {
                    val days = timeRemaining / AppUtil.DAY
                    if (days == 1L) {
                        timeRemainingStr = "Yesterday"
                    } else {
                        timeRemainingStr = String.format("%d " + if (days > 1) "days" else "day", days)
                    }
                } else if (timeRemaining < AppUtil.MONTH) {
                    val week = timeRemaining / AppUtil.WEEK
                    timeRemainingStr = String.format("%d " + if (week > 1) "weeks" else "week", week)
                } else if (timeRemaining < AppUtil.YEAR) {
                    val months = timeRemaining / AppUtil.MONTH
                    timeRemainingStr = String.format("%d " + if (months > 1) "months" else "month", months)
                } else {
                    val year = timeRemaining / AppUtil.YEAR
                    timeRemainingStr = String.format("%d " + if (year > 1) "years" else "year", year)
                }
                timeRemainingStr = timeRemainingStr + " ago"
            } catch (exception: Exception) {
                DebugHelper.trackException(AppUtil.TAG, exception)
            }

            return timeRemainingStr
        }

        fun hideKeyBoard(context: Context, editText: View) {

            val imm = context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)

        }

        /**
         * Hide soft keyboard.
         *
         * @param callingActivity the calling activity
         */
        fun hideSoftKeyboard(callingActivity: Activity) {
            if (callingActivity.currentFocus != null) {
                val inputManager = callingActivity
                        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(callingActivity
                        .currentFocus!!.windowToken,
                        InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }

        /**
         * Return date in specified format.
         *
         * @param milliSeconds Date in milliseconds
         * @param dateFormat   Date format
         * @return String representing date in specified format
         */
        fun getDateFromMiliSeconds(milliSeconds: Double,
                                   dateFormat: String): String {
            // Create a DateFormatter object for displaying date in specified
            // format.
            val lng = java.lang.Double.valueOf("" + milliSeconds)!!.toLong()
            // lng=(System.currentTimeMillis()*1000)-lng;
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in
            // milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = lng * 1000
            return formatter.format(calendar.time)
        }

        /**
         * Show soft keyboard.
         *
         * @param callActivity the call activity
         * @param view         the view
         */
        fun showSoftKeyboard(callActivity: Activity, view: View) {
            val imm = callActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }

        /**
         * Mathod to get date in message center required format. Like Today,
         * Yesterday and then in the format like Mon, 12 Mar 2015.
         *
         * @param dateTime
         * @param addTimeForTodayOrYest
         * @return
         */
        fun getDateTagWithTime(dateTime: String,
                               addTimeForTodayOrYest: Boolean): String {
            var dateTime = dateTime
            var dateOld = ""
            var formatCurr = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            if (dateTime.length > 18)
                dateTime = dateTime.substring(0, 18)
            else if (dateTime.length < 13)
                formatCurr = SimpleDateFormat("MMM dd, yyyy")
            try {

                val RFC1123_DATE_PATTERN = "EEE, dd MMM yyyy"
                val formatOldDate = SimpleDateFormat(
                        RFC1123_DATE_PATTERN)

                var dateStarter = ""
                val date = formatCurr.parse(dateTime)
                val calender = Calendar.getInstance()
                val currDate = calender.get(Calendar.DAY_OF_MONTH)
                if (date.date == currDate
                        && date.month == calender.get(Calendar.MONTH)
                        && date.year == calender.get(Calendar.YEAR) - 1900) {
                    dateStarter = "Today "

                    if (addTimeForTodayOrYest) {
                        val time = convertDateTimeTo12hourFormat(dateTime)
                        return dateStarter + time.toString()
                    }
                    return dateStarter
                }

                calender.add(Calendar.DAY_OF_MONTH, -1)
                val yesterDay = calender.get(Calendar.DAY_OF_MONTH)

                if (date.date == yesterDay
                        && date.month == calender.get(Calendar.MONTH)
                        && date.year == calender.get(Calendar.YEAR) - 1900) {
                    dateStarter = "Yesterday "

                    if (addTimeForTodayOrYest) {
                        val time = convertDateTimeTo12hourFormat(dateTime)
                        return dateStarter + time.toString()
                    }
                    return dateStarter
                }

                dateOld = formatOldDate.format(date)

            } catch (e: Exception) {
                Log.e(TAG, "Exception", e)
            }

            return dateOld
        }

        /**
         * Mathod to find out weather two given dates fall into same calendar day or
         * not.
         *
         * @param prev date
         * @param curr date
         * @return
         */
        fun isDateFallInSameDay(prev: Date?, curr: Date?): Boolean {
            if (prev == null)
                return false
            if (prev != null && curr != null) {
                val calCur = Calendar.getInstance()
                calCur.time = curr
                val calPrev = Calendar.getInstance()
                calPrev.time = prev
                if (calCur.get(Calendar.YEAR) == calPrev.get(Calendar.YEAR)
                        && calCur.get(Calendar.MONTH) == calPrev
                                .get(Calendar.MONTH)
                        && calCur.get(Calendar.DAY_OF_MONTH) == calPrev
                                .get(Calendar.DAY_OF_MONTH)) {
                    return true
                }
            }
            return false
        }

        val currentDateTimeString: String
            get() {
                val formatCurr = SimpleDateFormat(
                        "yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
                return formatCurr.format(Date(System.currentTimeMillis()))
            }

        /**
         * This method converts dp unit to equivalent pixels, depending on device
         * density.
         *
         * @param dp      A value in dp (density independent pixels) unit. Which we need
         * to convert into pixels
         * @param context Context to get resources and device specific display metrics
         * @return A float value to represent px equivalent to dp depending on
         * device density
         */
        fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return dp * (metrics.densityDpi / 160f)
        }

        /**
         * This method converts device specific pixels to density independent
         * pixels.
         *
         * @param px      A value in px (pixels) unit. Which we need to convert into db
         * @param context Context to get resources and device specific display metrics
         * @return A float value to represent dp equivalent to px value
         */
        fun convertPixelsToDp(px: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return px / (metrics.densityDpi / 160f)
        }
    }


}
