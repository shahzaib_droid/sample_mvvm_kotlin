package app.bizreview.me.utils;

import android.os.Environment;

import java.io.File;

import app.bizreview.me.App;

public class AppFolders {

    public static final String THUMBS = "thumbs";
    public static final String FILE_PATH_BASE = "/Android/data/app.bizreview.me/.files/";
    public static final String IMAGE_FILE_PATH = FILE_PATH_BASE + ".images/";
    private static final String TEMP_IMAGE_FOLDER = "/.temp_image/";


    public static String getRootPath() {
        File path = App.Companion.getInstance().getApplicationContext().getExternalFilesDir(null);
        if (path == null) {
            path = App.Companion.getInstance().getApplicationContext().getFilesDir();
        }
        return path.toString();
    }

    public static File getImageFilePath() {
        return new File(Environment.getExternalStorageDirectory().toString()
                + AppFolders.IMAGE_FILE_PATH);
    }

    public static String getRandomImageFilePath() {
        return Environment.getExternalStorageDirectory().toString()
                + AppFolders.IMAGE_FILE_PATH
                + (System.currentTimeMillis() / 1000) + ".jpg";
    }

    public static String getImagePath() {
        return Environment.getExternalStorageDirectory().toString()
                + AppFolders.IMAGE_FILE_PATH;
    }


    public static String getUserThumbnailPath() {
        return Environment.getExternalStorageDirectory()
                + AppFolders.FILE_PATH_BASE + AppFolders.THUMBS;

    }
}