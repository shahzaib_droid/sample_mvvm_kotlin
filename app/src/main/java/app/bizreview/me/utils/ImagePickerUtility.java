package app.bizreview.me.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import app.bizreview.me.App;


public class ImagePickerUtility {

    private static final int PIC_FROM_CAMERA = 1;
    private static final int PIC_FROM_FILE = 2;

    private Uri imageCaptureUri;

    private Activity mContext = null;
    private Fragment mFragmentContext = null;

    private String baseFolderPath = null;
    private String imageSourcePath = null;
    private String imageDestinationPath = null;
    private CameraRequest request;
    private ImagePickerListiner imagePickerListiner;


    public ImagePickerUtility(Activity mContext, CameraRequest request,
                              ImagePickerListiner listiner) {
        this.mContext = mContext;
        this.imagePickerListiner = listiner;
        this.request = request;
        createDirectory();
        if (this.request != null) {
            userActionRequest(request);
        }
    }

    public ImagePickerUtility(Activity activityContext, Fragment fragmentContext, CameraRequest request, ImagePickerListiner listiner) {
        this.mContext = activityContext;
        this.mFragmentContext = fragmentContext;
        this.imagePickerListiner = listiner;
        this.request = request;
        createDirectory();
        if (this.request != null) {
            userActionRequest(request);
        }
    }

    public void userActionRequest(CameraRequest request) {
        switch (request) {
            case CAPTURE_IMAGE_REQUEST:
                captureImageFromCamera();
                break;
            case CHOOSE_IMAGE_REQUEST:
                getImageFromGallery();
                break;
            default:
                break;
        }
    }

    private void startChildActivity(Intent intent, int result) {
        ((Activity) mContext).startActivityForResult(intent, result);
    }

    private void startChildActivityFragment(Intent intent, int result) {
        mFragmentContext.startActivityForResult(intent, result);
    }

    private void captureImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(AppFolders.getUserThumbnailPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            imageCaptureUri = FileProvider.getUriForFile(mContext, App.Companion.getAppContext().getPackageName() + ".provider", getRandomeFileName());
        } else {
            imageCaptureUri = Uri.fromFile(getRandomeFileName());
        }
        intent.putExtra("return-data", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                imageCaptureUri);
        if (mFragmentContext != null) {
            startChildActivityFragment(intent, PIC_FROM_CAMERA);
        } else {
            startChildActivity(intent, PIC_FROM_CAMERA);
        }
    }

    private File getRandomeFileName() {
        return new File(AppFolders.getImagePath(), getImageFileName());
    }

    private void getImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        PackageManager pm = mContext.getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
        int len = activityList.size();
        for (int i = 0; i < len; i++) {
            ResolveInfo app = (ResolveInfo) activityList.get(i);
            ActivityInfo activity = app.activityInfo;
            if (activity.packageName.contains("com.google.android.gallery")
                    || activity.packageName.contains("com.htc.album")
                    || activity.packageName.contains("android.gallery3d")) {
                intent.setClassName(activity.packageName, activity.name);
                break;
            }
        }
        if (mFragmentContext != null) {
            startChildActivityFragment(intent, PIC_FROM_FILE);
        } else {
            startChildActivity(intent, PIC_FROM_FILE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PIC_FROM_FILE
                        || requestCode == PIC_FROM_CAMERA) {
                    if (requestCode == PIC_FROM_FILE) {
                        checkImageUri(data);
                    } else if (requestCode == PIC_FROM_CAMERA) {
                        imageSourcePath = imageCaptureUri.getPath();
                    }
                    if (imageSourcePath != null && imageCaptureUri != null) {
                        resizeAndCompressBitmap(requestCode);
                    }
                }
            }
        } catch (NullPointerException exception) {

        }
    }

    public String getImageFileName() {
        return "image_" + String.valueOf(System.currentTimeMillis() / 1000)
                + ".jpg";
    }

    private void createDirectory() {
        if (baseFolderPath != null) {
            File file = new File(AppFolders.getUserThumbnailPath(), "");
            file.mkdirs();
        }
    }

    @SuppressWarnings("deprecation")
    private String getRealPathFromURI(String path) {

        Cursor cursor;
        if (path.contains("content://")) {

            Uri contentUri = Uri.parse(path);

            String[] proj = {MediaStore.Video.Media._ID,
                    MediaStore.Video.Media.DATA};
            cursor = ((Activity) mContext).managedQuery(contentUri, proj, null,
                    null, null);

            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);

        } else {
            return path;
        }
    }

    // private String getRealPathFromURI(Uri contentURI) {
    // String result;
    // Cursor cursor = mContext.getContentResolver().query(contentURI, null,
    // null, null, null);
    // if (cursor == null) { // Source is Dropbox or other similar local file
    // // path
    // result = contentURI.getPath();
    // } else {
    // cursor.moveToFirst();
    // int idx = cursor
    // .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
    // result = cursor.getString(idx);
    // cursor.close();
    // }
    // return result;
    // }

    private void sendDataToParent(String path, Uri imageUri) {
        imageSourcePath = path;
        imageCaptureUri = imageUri;
        if (imagePickerListiner != null) {
            imagePickerListiner.onImageReqeustCompleted(path, imageUri);
        }
    }

    private void checkImageUri(Intent data) {
        Uri mImageCaptureUri = data.getData();
        String mImagePath = getRealPathFromURI(mImageCaptureUri.toString());
        if (mImagePath != null) {
            this.imageCaptureUri = mImageCaptureUri;
            this.imageSourcePath = mImagePath;
        } else {
            this.imageCaptureUri = null;
            this.imageSourcePath = null;

        }
    }

    private void resizeAndCompressBitmap(int requestCode) {
        try {

            Bitmap bmp = UtilityImage.adjustImageOrientation(imageSourcePath);
            // Bitmap bmp = UtilityImage.resizeBitmapWithRatio(rotatedBitmap,
            // 100, 100);

            imageDestinationPath = imageSourcePath;
            if (requestCode == PIC_FROM_FILE) {
                imageDestinationPath = AppFolders.getRandomImageFilePath();//getRandomeFileName().getAbsolutePath();
            }
            File path = AppFolders.getImageFilePath();
            if(!path.exists()){
                path.mkdirs();
            }
            File file = new File(imageDestinationPath);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = null;

            fos = new FileOutputStream(file);
            if (fos != null) {
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                fos.close();
            }
            // rotatedBitmap.recycle();
            bmp.recycle();
            sendDataToParent(imageDestinationPath, imageCaptureUri);
        } catch (IOException exception) {
            DebugHelper.print("Image", exception, true);
        } catch (NullPointerException exception) {
            DebugHelper.print("Image", exception, true);
        }
    }

    public enum CameraRequest {
        CAPTURE_IMAGE_REQUEST, CHOOSE_IMAGE_REQUEST;

        public static CameraRequest getRequest(final int ordianl) {
            return CameraRequest.values()[ordianl];
        }
    }

    public interface ImagePickerListiner {
        void onImageReqeustCompleted(String filePath, Uri imageUri);
    }
}