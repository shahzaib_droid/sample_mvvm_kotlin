package app.bizreview.me.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UtilityImage {


    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;

    public static Bitmap resizeBitmap(final Bitmap bitmap, final int newWidth, final int newHeight) {
        final Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);
        if (bitmap != null) {
            final float ratioX = newWidth / (float) bitmap.getWidth();
            final float ratioY = newHeight / (float) bitmap.getHeight();
            final float middleX = newWidth / 2.0f;
            final float middleY = newHeight / 2.0f;

            final Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            final Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(
                    Paint.FILTER_BITMAP_FLAG));
        }
        return scaledBitmap;
    }

    public static Bitmap downloadImageBitmap(final String URL) {

        Bitmap bitmap = null;
        InputStream in = null;
        try {
            final URL imageUrl = new URL(URL);
            final HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            in = conn.getInputStream();

            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (final Exception exception) {
            DebugHelper.trackException(exception);
        }
        return bitmap;
    }

    public static Bitmap compressBitmap(Context mContext, Bitmap bitmap) {
        Bitmap result = bitmap;
        try {
            if (result.getWidth() > 480 && result.getHeight() > 640) {
                result = Bitmap.createScaledBitmap(result, 480, 640, false);
            }
        } catch (Exception exception) {
            DebugHelper.printException(exception);
        }
        return result;
    }


    //	public static InlineThumbnail bitmapToInlineThumbnail(String filePath) {
    //		InlineThumbnail inlineThumbnail = null;
    //		try {
    //			int typeCapture = AppPreference.getInt(FollowitApplication.getAppContext(), filePath + "-" + UtilityVideo.TYPE, -1);
    //
    //			Bitmap scaledImage = resizeBitmapWithRatio(
    //					getVideoThumbnail(filePath, typeCapture == UtilityVideo.FRONT_CAM, typeCapture == UtilityVideo.GALLERY), 10,
    //					10);
    //			int bytes = scaledImage.getByteCount();
    //			ByteBuffer buffer = ByteBuffer.allocate(bytes);
    //			scaledImage.copyPixelsToBuffer(buffer);
    //			inlineThumbnail = new InlineThumbnail(scaledImage.getWidth() + "x" + scaledImage.getHeight(), buffer.array());
    //		} catch (Exception exception) {
    //			DebugHelper.printException(exception);
    //		}
    //		return inlineThumbnail;
    //	}
    //
    //	public static Bitmap inlineThumbnailToBitmap(InlineThumbnail thumbnail) {
    //		if (thumbnail != null) {
    //			try {
    //				String dimens[] = thumbnail.getSize().split("x");
    //				int width = Integer.parseInt(dimens[0]);
    //				int height = Integer.parseInt(dimens[1]);
    //
    //				Bitmap bmp = Bitmap.createBitmap(width, height, Config.ARGB_8888);
    //				ByteBuffer buffer = ByteBuffer.wrap(thumbnail.getData());
    //				bmp.copyPixelsFromBuffer(buffer);
    //				return bmp;
    //			} catch (Exception exception) {
    //				DebugHelper.printException(exception);
    //			}
    //		}
    //		return null;
    //	}

    //	public static byte[] base64ToByteArray(String bytes) {
    //		return Base64.decode(bytes);
    //	}

    public static Bitmap flipImage(Bitmap src, int type) {
        Matrix matrix = new Matrix();
        if (type == FLIP_VERTICAL) {
            matrix.preScale(1.0f, -1.0f);
        } else if (type == FLIP_HORIZONTAL) {
            matrix.preScale(-1.0f, 1.0f);
        } else {
            return null;
        }
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        if (angle == 0 || angle == 180) {
            matrix.postRotate(angle + 90);
        }
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Bitmap adjustImageOrientation(String filePath) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap imageBitmaps = BitmapFactory.decodeFile(filePath, options);

        Bitmap finalImage = imageBitmaps;
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int rotate = 0;
            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }

            if (rotate != 0) {
                int w = imageBitmaps.getWidth();
                int h = imageBitmaps.getHeight();

                // Setting pre rotate
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);

                // Rotating Bitmap & convert to ARGB_8888, required by tess
                finalImage = Bitmap.createBitmap(imageBitmaps, 0, 0, w, h, mtx, false);
                imageBitmaps.recycle();
            }
        } catch (Exception exception) {
            DebugHelper.printException(exception);
        }
        return finalImage;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        try {

            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        } catch (Exception exception) {
            DebugHelper.printException(exception);
        }
        return null;
    }

    //	public static Bitmap adjustImageOrientation(String filePath) {
    //
    //		BitmapFactory.Options options = new BitmapFactory.Options();
    //		options.inSampleSize = 2;
    //
    //		Bitmap imageBitmaps = BitmapFactory.decodeFile(filePath, options);
    //
    //		Bitmap finalImage = imageBitmaps;
    //		ExifInterface exif;
    //		try {
    //			exif = new ExifInterface(filePath);
    //			int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    //
    //			int rotate = 0;
    //			switch (exifOrientation) {
    //			case ExifInterface.ORIENTATION_ROTATE_90:
    //				rotate = 90;
    //				break;
    //
    //			case ExifInterface.ORIENTATION_ROTATE_180:
    //				rotate = 180;
    //				break;
    //
    //			case ExifInterface.ORIENTATION_ROTATE_270:
    //				rotate = 270;
    //				break;
    //			}
    //
    //			if (rotate != 0) {
    //				int w = imageBitmaps.getWidth();
    //				int h = imageBitmaps.getHeight();
    //
    //				// Setting pre rotate
    //				Matrix mtx = new Matrix();
    //				mtx.preRotate(rotate);
    //
    //				// Rotating Bitmap & convert to ARGB_8888, required by tess
    //				finalImage = Bitmap.createBitmap(imageBitmaps, 0, 0, w, h, mtx, false);
    //				imageBitmaps.recycle();
    //			}
    //		} catch (Exception exception) {
    //		}
    //		return finalImage;
    //	}
    //	public static Bitmap resizeBitmapWithRatio(final Bitmap bitmap, final int newHeight, final int newWidth) {
    //
    //		try {
    //
    //			final int sourceWidth = bitmap.getWidth();
    //			final int sourceHeight = bitmap.getHeight();
    //			final float xScale = (float) newWidth / sourceWidth;
    //			final float yScale = (float) newHeight / sourceHeight;
    //			final float scale = Math.max(xScale, yScale);
    //
    //			// get the resulting size after scaling
    //			final float scaledWidth = scale * sourceWidth;
    //			final float scaledHeight = scale * sourceHeight;
    //
    //			// figure out where we should translate to
    //			final float dx = (newWidth - scaledWidth) / 2;
    //			final float dy = (newHeight - scaledHeight) / 2;
    //
    //			final Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
    //			final Canvas canvas = new Canvas(dest);
    //			final Matrix matrix = new Matrix();
    //			matrix.postScale(scale, scale);
    //			matrix.postTranslate(dx, dy);
    //			canvas.drawBitmap(bitmap, matrix, null);
    //			return dest;
    //		} catch (final OutOfMemoryError e) {
    //		} catch (Exception exception) {
    //		}
    //		return null;
    //	}


}
