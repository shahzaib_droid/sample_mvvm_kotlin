package app.bizreview.me.utils;

public class Const {

    public static final int C_3000 = 3000;
    public static final int C_8192 = 8192;
    public static final int C_9999 = 9999;
    public static final int C_9 = 9;
    public static final int C_11 = 11;
    public static final int C_8 = 8;
    public static final int C_25 = 25;
    public static final int C_30 = 30;
    public static final int C_12 = 12;
    public static final int C_15 = 15;
    public static final int C_14 = 14;
    public static final int C_10 = 10;
    public static final int C_20 = 20;
    public static final int C_1 = 1;
    public static final int C_300 = 300;
    public static final byte C_0xFF = (byte) 0xFF;
    public static final byte C_0x10 = (byte) 0x10;
    public static final int C_100 = 100;
    public static final int C_50 = 50;
    public static final long C_1024 = 1024;
    public static final int C_24 = 24;
    public static final int C_2 = 2;
    public static final int C_7 = 7;
    public static final int C_200 = 200;
    public static final int C_1000 = 1000;
    public static final int C_60 = 60;
    public static final int C_16 = 16;
    public static final int C_6 = 6;
    public static final int C_90 = 90;

    public static final int C_3 = 3;
    public static final int C_400 = 400;
    public static final int C_265 = 265;
    public static final int C_800 = 800;
    public static final int C_600 = 600;
    public static final int C_4 = 4;
    public static final int C_3600 = 3600;
    public static final int C_20000 = 20000;
    public static final int C_86400 = 86400;
    public static final int C_5 = 5;
    public static final double C_2_2 = 2.2d;
    public static final double C_0_5 = 0.5d;
    public static final double C_0_0 = 0.0d;
    public static final float C_0_3_5 = 0.35f;
    public static final float C_0_7_5 = 0.75f;
    public static final int C_1900 = 1900;
    public static final long C_3600000 = 3600000;
    public static final long C_1000000 = 1000000;

}
