package app.bizreview.me

import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import com.crashlytics.android.Crashlytics
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import io.fabric.sdk.android.Fabric
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.io.File

/**
 * Created by shahzaib on 11/06/17.
 */
class App : Application() {

    /*
     bae application class
     */
    private var defaultSubscribeScheduler: Scheduler? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        Fabric.with(this, Crashlytics())
        initAqueryCacheLocation()

        // initialization of image loader library

        val config = ImageLoaderConfiguration.Builder(
                this).memoryCache(WeakMemoryCache())
                .memoryCacheSize(50 * 1024 * 1024)
                .discCache(UnlimitedDiscCache(Companion.cacheDir))
                .discCacheSize(50 * 1024 * 1024).build()
        ImageLoader.getInstance().init(config);

    }

    fun checkIfHasNetwork(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun defaultSubscribeScheduler(): Scheduler? {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io()
        }
        return defaultSubscribeScheduler
    }


    companion object {

        // global methods

        var instance: App? = null
        private var cacheDir: File? = null
        val THUMBS = "thumbs"
        val FILE_PATH_BASE = "/Android/data/app.bizreview.me/.files/"
        val IMAGE_FILE_PATH = FILE_PATH_BASE + ".images/"
        private val TEMP_IMAGE_FOLDER = "/.temp_image/"


        fun hasNetwork(): Boolean {
            return instance!!.checkIfHasNetwork()
        }

        public fun getAppContext(): Context {
            return instance as Context
        }

        public fun getApplicationInstance(): Application {
            return instance as Application
        }

        public fun getAppResources(): Resources {
            return instance?.getResources()!!
        }

        private fun initAqueryCacheLocation() {
            cacheDir = File(getRootPath())
        }

        fun getAQCacheDir(): File? {
            return App.cacheDir
        }

        fun getRootPath(): String {
            var path = App.instance?.getApplicationContext()?.getExternalFilesDir(null)
            if (path == null) {
                path = App.instance?.getApplicationContext()?.getFilesDir()
            }
            return path!!.toString()
        }

    }
}