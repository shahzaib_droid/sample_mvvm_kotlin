package app.bizreview.me.adpaters

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.util.*

abstract class BaseAdapter internal constructor(context: Context,
                                                resource: Int,
                                                textView: Int, objects: ArrayList<Any>)
    : ArrayAdapter<Any>(context,
        resource,
        textView, objects) {

    val tag = this.javaClass.name

    protected var mContext: Context? = null

    protected var mAllData: ArrayList<Any>? = null

    var inflater: LayoutInflater? = null

    var screenHandler: Handler? = null


    var data: ArrayList<Any>?
        get() = this.mAllData
        set(mDataList) {
            if (this.mAllData != null)
                this.mAllData = ArrayList()
            this.mAllData = mDataList
        }

    init {
        initData(context, objects)
    }

    internal fun initData(mContext: Context?, mDataList: ArrayList<Any>) {
        this.mContext = mContext
        this.mAllData = mDataList
        if (mContext != null) {
            this.inflater = this.mContext!!
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
    }

    override fun getContext(): Context {
        return this.mContext!!
    }

    fun setContext(mContext: Context) {
        this.mContext = mContext
    }

    fun appendData(mDataList: ArrayList<Any>?) {
        if (mDataList != null && !mDataList.isEmpty()) {
            if (mAllData == null) {
                mAllData = mDataList
            } else {
                mAllData!!.addAll(mDataList)
            }
        }
        notifyDataSetChanged()
    }

    fun setHanlder(handler: Handler) {
        this.screenHandler = this.screenHandler
    }

    override fun getCount(): Int {

        return if (this.mAllData != null) {
            this.mAllData!!.size
        } else 0
    }

    override fun getItem(index: Int): Any? {
        return if (this.mAllData != null) {
            this.mAllData!![index]
        } else null
    }

    override fun getItemId(index: Int): Long {

        return 0
    }

    fun loadImage(imageView: ImageView, imageURL: String) {
        Glide.with(context!!)
                .load(imageURL)
                .into(imageView);
    }
}
