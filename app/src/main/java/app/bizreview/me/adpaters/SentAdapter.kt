package app.bizreview.me.adpaters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import app.bizreview.me.R
import app.bizreview.me.data.model.sentReviews.Data
import app.bizreview.me.databinding.RowSentMessageBinding
import app.bizreview.me.viewholders.ViewHolderSent
import java.util.*

/**
 * Created by shahzaib on 3/20/2018.
 */
class SentAdapter(var mContext: Context?) : RecyclerView.Adapter<ViewHolderSent>(), Filterable {


    var reviews: MutableList<Data?>? = mutableListOf()
    var filteredReviews: MutableList<Data?>? = mutableListOf()

    override fun onBindViewHolder(holder: ViewHolderSent, position: Int) {
        //  updating each row view
        holder.updateView(filteredReviews?.get(position))
    }

    override fun getItemCount(): Int {
        return filteredReviews?.size!!
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSent {
        val mBinding: RowSentMessageBinding = DataBindingUtil.inflate<RowSentMessageBinding>(LayoutInflater.from(mContext)
                , R.layout.row_sent_message, null, false)
        return ViewHolderSent(mBinding, mContext)
    }

    // method to filter the list for search results
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                filteredReviews?.clear()
                val charString = charSequence.toString().trim { it <= ' ' }
                if (charString.isEmpty()) {
                    filteredReviews?.addAll(reviews!!)
                } else {
                    val filteredList = ArrayList<Data?>()
                    for (row in reviews!!) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row?.response?.toLowerCase()?.contains(charString.toLowerCase(), true)!!
                                || row.to?.toLowerCase()?.contains(charString.toLowerCase(), true)!!) {
                            filteredList.add(row)
                        }
                    }

                    filteredReviews = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredReviews
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredReviews = filterResults.values as ArrayList<Data?>
                // refresh the list with filtered data
                notifyDataSetChanged()
            }
        }
    }
}