package app.bizreview.me.ui.viewModel

import android.content.Context
import android.databinding.ObservableInt
import android.view.View
import app.bizreview.me.ui.HomeActivity

/**
 * Created by shahzaib on 3/13/2018.
 */
class HomeViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {

    /*
     bottom bar home view model
     */

    var progressVisibility: ObservableInt

    init {
        progressVisibility = ObservableInt(View.INVISIBLE)
    }

    override fun destroy() {
        context = null
        dataListener = null
    }

    fun onClickBack(): View.OnClickListener {
        return View.OnClickListener {
            (context as HomeActivity).onBackPressed()
        }
    }

    fun onClickSend(): View.OnClickListener {
        return View.OnClickListener {
            dataListener?.onSend()
        }
    }

    fun onClickRespond(): View.OnClickListener {
        return View.OnClickListener {
            dataListener?.onRespond()
        }
    }


    interface DataListener {
        fun onSend()

        fun onRespond()
    }
}

