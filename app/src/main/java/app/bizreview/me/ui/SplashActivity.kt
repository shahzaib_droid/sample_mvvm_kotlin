package app.bizreview.me.ui

import android.Manifest
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.CountDownTimer
import android.support.design.widget.Snackbar
import app.bizreview.me.R
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.model.refreshToken.RefreshTokenResponse
import app.bizreview.me.databinding.ActivitySplashBinding
import app.bizreview.me.ui.viewModel.SplashViewModel
import app.bizreview.me.utils.AppConstants
import app.bizreview.me.utils.AppPreference
import app.bizreview.me.utils.AppWebServices
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

class SplashActivity : BizReviewMeActivity(), SplashViewModel.DataListener, EasyPermissions.PermissionCallbacks {

    lateinit var binding: ActivitySplashBinding
    lateinit var viewModel: SplashViewModel

    //Permission
    private var ASK_PERMISSIONS: Int? = 10
    private val PERMISSIONS = arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_EXTERNAL_STORAGE)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        viewModel = SplashViewModel(this, this)
        binding.model = viewModel

        object : CountDownTimer(SPLASH_TIME, SPLASH_TIME) {

            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                methodRequiresPermission()
            }
        }.start()
    }


    fun checkLogin() {
        if (AppPreference.getSavedData(this@SplashActivity, AppConstants.KEY_IS_LOGGED)) {
            checkTokenIfExpire()
        } else {
            startLogin()
            finish()
        }
//        finish()
    }

    //Permissions
    @AfterPermissionGranted(10)
    private fun methodRequiresPermission() {
        if (EasyPermissions.hasPermissions(this, *PERMISSIONS)) {
            checkLogin()
        } else {
            EasyPermissions.requestPermissions(this, "Application requires following permissions to work properly", ASK_PERMISSIONS!!, *PERMISSIONS)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>?) {
        var permission = ""
        if (perms != null) {
            for (i in perms.indices) {
                permission += perms[i]
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>?) {
        var permissionsName = ""
        var permission = ""
        if (perms != null /* click listener */) {
            for (i in perms.indices) {
                permission += perms[i]
                permissionsName += perms[i] + "\n"
            }
        }
        AppSettingsDialog.Builder(this, getString(R.string.rationale_ask_again) + "\n" + permissionsName)
                .setTitle(getString(R.string.title_settings_dialog))
                .setPositiveButton(getString(R.string.setting))
                .setNegativeButton(getString(R.string.cancel), null)
                .setRequestCode(ASK_PERMISSIONS!!)
                .build()
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun checkTokenIfExpire() {
        if (AppPreference.getValue(this, AppConstants.KEY_USER_TOKEN_EXPIRE) != null) {
            val statTime = java.lang.Long.parseLong(AppPreference.getValue(this, AppConstants.KEY_USER_TOKEN_EXPIRE))
            val currentTime = System.currentTimeMillis() / 1000
            var timeRemaining = statTime - currentTime
            if (timeRemaining > 1 && timeRemaining <= 50000) {
                //refresh
                viewModel.refreshToken()

            } else if (timeRemaining < 1) {
                //login
                viewModel.login(AppPreference.getValue(this, AppConstants.KEY_EMAIL),
                        AppPreference.getValue(this, AppConstants.KEY_USER_PASSWORD))

            } else {
                // showing drawer home once and bottom bar home once
                if (AppPreference.getSavedData(this, AppConstants.KEY_IS_DRAWER_HOME)) {
                    AppPreference.saveData(this,false, AppConstants.KEY_IS_DRAWER_HOME)
                    startHome()
                } else {
                    AppPreference.saveData(this,true, AppConstants.KEY_IS_DRAWER_HOME)
                    startBottomBarHome()
                }
            }
        } else {
            AppPreference.saveData(this, false, AppConstants.KEY_IS_LOGGED)
        }
    }

    private fun startBottomBarHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startHome() {
        val intent = Intent(this, SlidingHomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    public fun startLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRefreshSuccess(response: RefreshTokenResponse) {
        AppPreference.saveValue(this, response?.token, AppWebServices.KEY_USER_ACCESS_TOKEN)
        AppPreference.saveValue(this, response?.expires.toString(), AppConstants.KEY_USER_TOKEN_EXPIRE)
        startHome()
    }

    override fun onMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onLoginSuccess(response: LoginResponse) {
        AppPreference.saveData(this, true, AppConstants.KEY_IS_LOGGED)
        AppPreference.saveValue(this, response?.data?.token, AppWebServices.KEY_USER_ACCESS_TOKEN)
        AppPreference.saveValue(this, response?.data?.expires.toString(), AppConstants.KEY_USER_TOKEN_EXPIRE)
        AppPreference.saveValue(this, response.data?.userinfo?.first_name, AppConstants.KEY_FIRST_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.last_name, AppConstants.KEY_LAST_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.fullname, AppConstants.KEY_FULL_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.email, AppConstants.KEY_EMAIL)
        AppPreference.saveValue(this, response.data?.userinfo?.id, AppConstants.KEY_USER_ID)
        startHome()
    }

    companion object {
        val SPLASH_TIME: Long = 3000
    }
}
