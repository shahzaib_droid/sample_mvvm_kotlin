package app.bizreview.me.ui.viewModel

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.model.refreshToken.RefreshTokenResponse
import app.bizreview.me.ui.HomeActivity
import app.bizreview.me.ui.SplashActivity
import app.bizreview.me.utils.AppPreference
import app.bizreview.me.utils.AppWebServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

/**
 * Created by shahzaib on 3/5/2018.
 */
class SplashViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {


    lateinit var mProgressDialog: ProgressDialog

    var disposable: DisposableObserver<LoginResponse>? = null
    var response: LoginResponse? = null

    var disposableRefresh: DisposableObserver<RefreshTokenResponse>? = null
    var responseRefresh: RefreshTokenResponse? = null

    override fun destroy() {

        if (disposable?.isDisposed != null &&
                !disposable?.isDisposed!!) disposable!!.dispose()
        disposable = null
        context = null
        dataListener = null
    }

    fun login(username: String?, password: String?) {
        val injector = Injector()
        val api = injector.provideApi()
        disposable = api.login(username, password, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    // refresh token api call
    fun refreshToken() {
        val injector = Injector()
        val api = injector.provideApi()
        disposableRefresh = api.refreshToken(AppPreference.getValue(context, AppWebServices.KEY_USER_ACCESS_TOKEN),
                720.00, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(RefreshObserver())
    }

    private inner class LongOperationObserver : DisposableObserver<LoginResponse>() {

        override fun onNext(value: LoginResponse) {
            this@SplashViewModel.response = value
        }

        override fun onError(e: Throwable) {
//            hideProgressDialog()
            dataListener?.onMessage(e.localizedMessage)
            (context as SplashActivity).startLogin()
        }

        override fun onComplete() {
            if (response?.status?.equals("success", true)!!) {
                dataListener?.onLoginSuccess(response!!)
            } else {
                dataListener?.onMessage(response?.message!!)
                (context as SplashActivity).startLogin()
            }
//            hideProgressDialog()
        }
    }

    private inner class RefreshObserver : DisposableObserver<RefreshTokenResponse>() {

        override fun onNext(value: RefreshTokenResponse) {
            this@SplashViewModel.responseRefresh = value
        }

        override fun onError(e: Throwable) {
            dataListener?.onMessage(e.localizedMessage)
            (context as SplashActivity).startLogin()
        }

        override fun onComplete() {
            if (responseRefresh?.status?.equals("success", true)!!) {
                dataListener?.onRefreshSuccess(responseRefresh!!)
            } else {
                dataListener?.onMessage(responseRefresh?.message!!)
                (context as SplashActivity).startLogin()
            }
        }
    }

    //Show Loader
    fun showProgressDialog(text: String, progress: Int) {
        mProgressDialog = ProgressDialog(context, R.style.AppCompatAlertDialogStyle)
        mProgressDialog.setMessage(text)
        mProgressDialog.setIndeterminate(false)
        mProgressDialog.setMax(progress)
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    //hide progress
    fun hideProgressDialog() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    fun startHome() {
        val intent = Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }

    //interface
    interface DataListener {
        fun onLoginSuccess(response: LoginResponse)
        fun onRefreshSuccess(response: RefreshTokenResponse)
        fun onMessage(message: String)
    }


}