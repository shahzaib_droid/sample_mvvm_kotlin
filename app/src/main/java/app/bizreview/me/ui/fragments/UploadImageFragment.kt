package app.bizreview.me.ui.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import app.bizreview.me.R
import app.bizreview.me.data.model.addPlace.AddPlaceResponse
import app.bizreview.me.databinding.FragmentUploadImageBinding
import app.bizreview.me.ui.CropperActivity
import app.bizreview.me.ui.SlidingHomeActivity
import app.bizreview.me.ui.viewModel.UploadImageViewModel
import app.bizreview.me.utils.*
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.JsonElement
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import com.soundcloud.android.crop.Crop
import okhttp3.RequestBody
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

/**
 * Created by shahzaib on 3/13/2018.
 */
class UploadImageFragment() : Fragment(), UploadImageViewModel.DataListener,
        ImagePickerUtility.ImagePickerListiner, View.OnClickListener, EasyPermissions.PermissionCallbacks {

    lateinit var binding: FragmentUploadImageBinding
    lateinit var model: UploadImageViewModel

    //Layout
    private val dialog: MaterialDialog? = null
    private val mLlCamera: LinearLayout? = null
    private val mLlFile: LinearLayout? = null

    //Local
    private var imagePickerDialog: ImagePickerUtility? = null
    private var mIsChooseFromGallery = false
    private var path: String? = null
    private var mProgressDialog: ProgressDialog? = null

    //Permission
    private var ASK_PERMISSIONS: Int? = null
    private val PERMISSIONS_CAMERA = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate<FragmentUploadImageBinding>(LayoutInflater.from(activity), R.layout.fragment_upload_image, null, false)
        model = UploadImageViewModel(activity, this, false)
        binding.model = model
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ASK_PERMISSIONS = 10
        imagePickerDialog = ImagePickerUtility(activity, this, null, this)

//        UtilityImageLoader.showRoundedImageWithNoBackground(binding.ivLocation, place?.logo)

    }

    override fun onResume() {
        super.onResume()
        // set header text in on resume
        (activity as SlidingHomeActivity).setHeaderText("Add Place")

    }

    override fun onDestroy() {
        super.onDestroy()
        model.destroy()
    }

    //helping methods
    fun bottomDialogImageSelection() {
        val dialogPlus = DialogPlus.newDialog(activity)
                .setContentHolder(ViewHolder(R.layout.dialog_image_selection))
                .setCancelable(true)
                .setBackgroundColorResId(R.color.transparent)
                .setOnDismissListener { }
                .setOnCancelListener { }
                .setOnBackPressListener { }
                .create()
        val mTvPhoto = dialogPlus.findViewById(R.id.tv_photo) as TextView
        val mTvFile = dialogPlus.findViewById(R.id.tv_file) as TextView
        val mTvCancelDialog = dialogPlus.findViewById(R.id.tv_cancel) as TextView

        mTvPhoto.setOnClickListener {
            dialog?.cancel()
            mIsChooseFromGallery = true
            imagePickerDialog?.userActionRequest(ImagePickerUtility.CameraRequest.CAPTURE_IMAGE_REQUEST)
            dialogPlus.dismiss()
        }

        mTvFile.setOnClickListener {
            dialog?.cancel()
            mIsChooseFromGallery = true
            imagePickerDialog?.userActionRequest(ImagePickerUtility.CameraRequest.CHOOSE_IMAGE_REQUEST)
            dialogPlus.dismiss()
        }
        mTvCancelDialog.setOnClickListener { dialogPlus.dismiss() }
        dialogPlus.show()
    }


    //Permissions
    @AfterPermissionGranted(10)
    private fun methodRequiresPermission() {
        if (EasyPermissions.hasPermissions(activity!!, *PERMISSIONS_CAMERA)) {
            bottomDialogImageSelection()
        } else {
            // show message if permission not guaranteed
            EasyPermissions.requestPermissions(this, "Application requires following permissions to work properly", ASK_PERMISSIONS!!, *PERMISSIONS_CAMERA)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>?) {
        var permission = ""
        if (perms != null) {
            for (i in perms.indices) {
                permission += perms[i]
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>?) {
        var permissionsName = ""
        var permission = ""
        if (perms != null /* click listener */) {
            for (i in perms.indices) {
                permission += perms[i]
                permissionsName += perms[i] + "\n"
            }
        }
        AppSettingsDialog.Builder(this, getString(R.string.rationale_ask_again) + "\n" + permissionsName)
                .setTitle(getString(R.string.title_settings_dialog))
                .setPositiveButton(getString(R.string.setting))
                .setNegativeButton(getString(R.string.cancel), null)
                .setRequestCode(ASK_PERMISSIONS!!)
                .build()
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //passing permission result to easy permission library
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ASK_PERMISSIONS) {
            Toast.makeText(activity, R.string.returned_from_app_settings_to_activity, Toast.LENGTH_SHORT).show()
            methodRequiresPermission()
        } else if (resultCode == RESULT_OK && requestCode == 100) {
            if (data != null) {
                showProfileImage(Crop.getOutput(data))
            }
        } else {
            if (mIsChooseFromGallery) {
                if (imagePickerDialog != null) {
                    imagePickerDialog?.onActivityResult(requestCode, resultCode, data)
                }
                mIsChooseFromGallery = false
            }
        }
    }

    // showing selected image
    private fun showProfileImage(data: Uri?) {
        if (data != null) {
            path = data.path
            path = AppUtil.saveHDFile(path!!)
            model.imagePath = path
            binding.ivLocation.setBackground(null)
            binding.ivLocation.setImageBitmap(AppUtil.decodeAdjustedFileHD(path))
        }
    }

    // sending image to cropper library
    override fun onImageReqeustCompleted(filePath: String?, imageUri: Uri?) {
        // TODO Auto-generated method stub
        if (filePath != null) {
            path = filePath
            if (imageUri != null) {
                val data = Intent(activity, CropperActivity::class.java)
                data.putExtra(AppBundles.CROPPER_IMAGE.getKey(), imageUri)
                startActivityForResult(data, 100)
            }

        }
    }

    override fun onClick(v: View) {
        val intent: Intent? = null
        when (v.id) {

        }
    }

    // called on succefully adding a plae
    override fun onAddSuccess(response: AddPlaceResponse?) {
        onMessage(response?.status!!)
    }

    // method called to select image
    override fun onImage() {
        methodRequiresPermission()
    }

    // snack bar notification
    override fun onMessage(message: String) {
        Snackbar.make(binding.rlRoot, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onUploadWithProgress(path: String?) {
        showProgressDialog("Uploading image...", 100, true)
        SendFileTask(path).execute()
    }

    private fun showProgressDialog(text: String, progress: Int, showHorizontal: Boolean) {
        mProgressDialog = ProgressDialog(activity, R.style.AppCompatAlertDialogStyle)
        mProgressDialog?.setMessage(text)
        mProgressDialog?.setIndeterminate(false)
        mProgressDialog?.setMax(progress)
        if (showHorizontal) {
            mProgressDialog?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        } else {
            mProgressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        }
        mProgressDialog?.setCancelable(false)
        mProgressDialog?.show()
    }

    //Request
    inner class SendFileTask(var path: String?) : AsyncTask<Void, Int, Boolean>(), ProgressRequestBody.UploadCallbacks {

        var totalSize: Long = 0
        var message: String? = null
        var response: JsonElement? = null

        override fun doInBackground(vararg params: Void): Boolean? {
            totalSize = 0
            val policy = StrictMode.ThreadPolicy.Builder()
                    .permitAll().build()
            StrictMode.setThreadPolicy(policy)
            var temp: File? = null
            val body = HashMap<String, RequestBody>()
            body.put("place_id", NetworkUtility.toRequestBody("Xl4LEvt4lpudAMcb9q1vtQ"))
            body.put("name", NetworkUtility.toRequestBody("3 Blind Brothers"))
            body.put("notes", NetworkUtility.toRequestBody("Thank you for using 3 Blind Brothers, please let us know how you enjoyed our services."))
            var fileBody: ProgressRequestBody? = null
            if (path != null) {
                temp = File(path!!)
                totalSize += temp.length()
                fileBody = ProgressRequestBody(temp, this)
                body.put("file\"; filename=\"image.jpg\" ", fileBody)
            } else {

            }

            return true
        }

        override fun onPostExecute(result: Boolean?) {
            // TODO Auto-generated method stub
            super.onPostExecute(result)
        }


        override fun onProgressUpdate(percentage: Int) {
            try {
                if (mProgressDialog != null) {
                    mProgressDialog?.setProgress(percentage)
                }
            } catch (exception: Exception) {
                DebugHelper.printException(exception)
            }

        }

        override fun onError() {
            if (mProgressDialog != null) {
                mProgressDialog?.hide()
            }
        }

        override fun onFinish() {
            if (mProgressDialog != null) {
                mProgressDialog?.hide()
            }
        }
    }

}