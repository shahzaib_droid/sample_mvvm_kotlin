package app.bizreview.me.ui.viewModel

import android.content.Context
import android.databinding.ObservableInt
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.bizreview.me.App
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.info.InfoResponse
import app.bizreview.me.data.model.sentReviews.SentReviewsResponse
import app.bizreview.me.utils.AppConstants
import app.bizreview.me.utils.AppPreference
import app.bizreview.me.utils.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

/**
 * Created by shahzaib on 3/13/2018.
 */
class RespondViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {


    //local
    var editTextSearchValue: String? = null

    var progressVisibility: ObservableInt? = null
    var disposable: DisposableObserver<InfoResponse>? = null
    var response: InfoResponse? = null


    init {
        progressVisibility = ObservableInt(View.GONE)
    }

    // onClicks
    fun getSearchEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                editTextSearchValue = charSequence.toString()
                dataListener?.onQuery(editTextSearchValue)
            }

            override fun afterTextChanged(s: Editable) {

            }
        }
    }

    override fun destroy() {
        if(disposable != null) {
            if (!disposable!!.isDisposed) disposable!!.dispose()
        }
        disposable = null
        context = null
        dataListener = null
    }

    // info api call
    fun info() {
        progressVisibility?.set(View.VISIBLE)
        val injector = Injector()
        val api = injector.provideApi()
        disposable = api.info(AppUtil.getHeaders(context),
                Integer.parseInt(AppPreference.getValue(context, AppConstants.KEY_USER_ID)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    private inner class LongOperationObserver : DisposableObserver<InfoResponse>() {

        override fun onNext(value: InfoResponse) {
            response = value
        }

        override fun onError(e: Throwable) {
            dataListener?.onMessage(e.localizedMessage)
            progressVisibility?.set(View.GONE)

        }

        override fun onComplete() {
            progressVisibility?.set(View.GONE)
            if (response?.status?.equals("success", true)!!) {

                dataListener?.onSuccess(response!!)
            } else {
                dataListener?.onMessage(response?.message!!)
            }
        }
    }

    interface DataListener {
        fun onSuccess(response: InfoResponse)
        fun onMessage(message: String)
        fun onQuery(query : String?)
    }
}