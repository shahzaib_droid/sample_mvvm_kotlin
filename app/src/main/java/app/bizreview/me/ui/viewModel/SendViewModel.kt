package app.bizreview.me.ui.viewModel

import android.app.ProgressDialog
import android.content.Context
import android.databinding.ObservableInt
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.sentReviews.SentReviewsResponse
import app.bizreview.me.ui.HomeActivity
import app.bizreview.me.utils.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

/**
 * Created by shahzaib on 3/13/2018.
 */
class SendViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {

    //local
    var editTextSearchValue: String? = null

    var progressVisibility: ObservableInt? = null
    var disposable: DisposableObserver<SentReviewsResponse>? = null
    var response: SentReviewsResponse? = null
    lateinit var mProgressDialog: ProgressDialog

    init {
        progressVisibility = ObservableInt(View.GONE)
    }

    // onClicks
    fun getSearchEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                editTextSearchValue = charSequence.toString()
                dataListener?.onQuery(editTextSearchValue)
            }

            override fun afterTextChanged(s: Editable) {

            }
        }
    }

    fun onClickRequestReview(): View.OnClickListener {
        return View.OnClickListener {
        }
    }

    override fun destroy() {
        if(disposable != null) {
            if (!disposable!!.isDisposed) disposable!!.dispose()
        }
        disposable = null
        context = null
        dataListener = null
    }

    //sent reviews api call
    fun getSentReviews() {
        progressVisibility?.set(View.VISIBLE)
        val injector = Injector()
        val api = injector.provideApi()
        disposable = api.getSentReviews(AppUtil.getHeaders(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    private inner class LongOperationObserver : DisposableObserver<SentReviewsResponse>() {

        override fun onNext(value: SentReviewsResponse) {
            response = value
        }

        override fun onError(e: Throwable) {
            progressVisibility?.set(View.GONE)
            dataListener?.onMessage(e.localizedMessage)
        }

        override fun onComplete() {
            if (response?.status?.equals("success", true)!!) {
                dataListener?.onSuccess(response!!)
            } else {
                dataListener?.onMessage(response?.message!!)
            }
            progressVisibility?.set(View.GONE)
        }
    }

    //Show Loader
    fun showProgressDialog(text: String, progress: Int) {
        mProgressDialog = ProgressDialog(context, R.style.AppCompatAlertDialogStyle)
        mProgressDialog.setMessage(text)
        mProgressDialog.setIndeterminate(false)
        mProgressDialog.setMax(progress)
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    //hide progress
    fun hideProgressDialog() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    // inerface to communicate between model and activity
    interface DataListener {
        fun onSuccess(response: SentReviewsResponse)
        fun onMessage(message: String)
        fun onQuery(query : String?)
    }
}