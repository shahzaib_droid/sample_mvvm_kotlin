package app.bizreview.me.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View
import app.bizreview.me.R
import app.bizreview.me.databinding.ActivityHomeBinding
import app.bizreview.me.ui.fragments.*
import app.bizreview.me.ui.viewModel.HomeViewModel
import app.bizreview.me.utils.AppBundles

class HomeActivity : BizReviewMeActivity(), HomeViewModel.DataListener {

    /*
     Activity responsible for bootom bar menue
     */
    public lateinit var binding: ActivityHomeBinding
    lateinit var model: HomeViewModel

    private var fragment: Fragment? = null
    private var sendFragment: Fragment? = null
    private var respondFragment: Fragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        model = HomeViewModel(this, this)
        binding.model = model
        setSupportActionBar(binding.toolbar)
        onSend()
    }

    override fun onDestroy() {
        super.onDestroy()
        model.destroy()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount - 1 > 0) {
            supportFragmentManager.popBackStack()
        } else {
//            showExitDialog()
            super.onBackPressed()
        }
    }

    fun onFragmentSelected(clear: Boolean, tag: String) {
        if (fragment != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            if (clear) {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            } else {
                fragmentTransaction.addToBackStack(tag)
            }
            fragmentTransaction.replace(R.id.container_body, fragment)
            fragmentTransaction.commit()
        }
    }

    fun onReplaceFragment(fragment: Fragment?, Tag: String) {
        if (fragment != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container_body, fragment,
                    Tag)
            fragmentTransaction.addToBackStack(Tag)
            fragmentTransaction.commit()
        }
    }

    override fun onSend() {
        if (sendFragment == null) {
            sendFragment = SendFragment()
        }
        fragment = sendFragment
        onFragmentSelected(true, "sent")
        binding.ivRespond.isSelected = false
        binding.ivSent.isSelected = true
    }


    override fun onRespond() {
        if (respondFragment == null) {
            respondFragment = RespondFragment()
        }
        fragment = respondFragment
        onFragmentSelected(true, "respond")
        binding.ivRespond.isSelected = true
        binding.ivSent.isSelected = false
    }



    public fun setHeaderText(title: String) {
        binding.tvTitle.text = title
    }

    fun setBack(b: Boolean) {
        if (b) {
            binding.ivBack.visibility = View.VISIBLE
        } else {
            binding.ivBack.visibility = View.GONE
        }
    }

    fun onMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        var CURRENT_TAB: Int = 0
    }

}
