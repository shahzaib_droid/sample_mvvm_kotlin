package app.bizreview.me.ui.viewModel

import android.content.Context
import android.databinding.ObservableInt
import android.view.View
import app.bizreview.me.ui.HomeActivity

/**
 * Created by shahzaib on 3/13/2018.
 */
class SlidingHomeViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {

    // home view model for slider activity

    var progressVisibility: ObservableInt

    init {
        progressVisibility = ObservableInt(View.INVISIBLE)
    }

    override fun destroy() {
        context = null
        dataListener = null
    }

    fun onClickBack(): View.OnClickListener {
        return View.OnClickListener {
            (context as HomeActivity).onBackPressed()
        }
    }



    // inerface to communicate between model and activity
    interface DataListener {

    }
}