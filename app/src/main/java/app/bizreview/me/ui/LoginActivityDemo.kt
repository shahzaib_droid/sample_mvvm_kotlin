package app.bizreview.me.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import app.bizreview.me.R
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.databinding.ActivityLoginBinding
import app.bizreview.me.ui.viewModel.LoginViewModel
import app.bizreview.me.utils.AppPreference
import app.bizreview.me.utils.AppWebServices

/**
 * A login screen that offers login via email/password.
 */
class LoginActivityDemo : AppCompatActivity(), LoginViewModel.DataListener {

    lateinit var binding: ActivityLoginBinding
    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = LoginViewModel(this, this)
        binding.model = viewModel

    }

    override fun onLoginSuccess(response: LoginResponse) {
        AppPreference.saveValue(this, response?.data?.token, AppWebServices.KEY_USER_ACCESS_TOKEN)
        viewModel.startHome()
    }

    override fun onMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}
