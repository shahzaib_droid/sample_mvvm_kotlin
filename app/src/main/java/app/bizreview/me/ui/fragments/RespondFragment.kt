package app.bizreview.me.ui.fragments

import android.Manifest
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.model.info.InfoResponse
import app.bizreview.me.data.model.sentReviews.Data
import app.bizreview.me.data.model.sentReviews.SentReviewsResponse
import app.bizreview.me.databinding.FragmentRespondBinding
import app.bizreview.me.ui.HomeActivity
import app.bizreview.me.ui.viewModel.RespondViewModel
import app.bizreview.me.utils.AppUtil
import app.bizreview.me.utils.SpaceItemDecoration
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

/**
 * Created by shahzaib on 3/13/2018.
 */
class RespondFragment : Fragment(), RespondViewModel.DataListener {

    lateinit var binding: FragmentRespondBinding
    lateinit var model: RespondViewModel

    private val ASK_PERMISSIONS = 20
    private val PERMISSIONS_STREAM = arrayOf(Manifest.permission.READ_CONTACTS)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate<FragmentRespondBinding>(LayoutInflater.from(activity), R.layout.fragment_respond, null, false)
        model = RespondViewModel(activity, this)
        binding.model = model
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model.info()
    }

    override fun onResume() {
        super.onResume()
        (activity as HomeActivity).setHeaderText("Responded")
        (activity as HomeActivity).setBack(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        model.destroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    //Permissions
    @AfterPermissionGranted(20)
    private fun methodRequiresPermission() {
        if (EasyPermissions.hasPermissions(context!!, *PERMISSIONS_STREAM)) {
            onPermissionGranted()
        } else {
            EasyPermissions.requestPermissions(this, "Application requires following permissions to work properly", ASK_PERMISSIONS, *PERMISSIONS_STREAM)
        }
    }

    fun onPermissionGranted() {
        model.info()
    }

    //helping methods

    private fun setHintForSearch() {
        binding.etSearch.hint = AppUtil.getDecoratedHint(binding.etSearch,
                "Search", App.getAppResources().getDrawable(R.drawable.ic_search))
    }

    override fun onSuccess(response: InfoResponse) {
       onMessage("Get Info API response ${response.status}")
    }

    override fun onMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onQuery(query: String?) {
    }
}