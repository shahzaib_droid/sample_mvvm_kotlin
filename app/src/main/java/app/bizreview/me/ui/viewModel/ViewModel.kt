package app.bizreview.me.ui.viewModel

/**
 * Created by shahzaib on 10/06/17.
 */
// base interface which is implemented by each viewModel
interface ViewModel {

    fun destroy()
}