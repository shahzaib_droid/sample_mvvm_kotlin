package app.bizreview.me.ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import app.bizreview.me.R


open class BizReviewMeActivity : AppCompatActivity() {

    protected var mContext: Context? = null
    protected var TAG = javaClass.name
    private var mToolbar: Toolbar? = null
    private var mCallingActivity: Activity? = null
    private var mTitle: TextView? = null


    protected fun onCreate(savedInstanceState: Bundle, currentActivity: Activity) {
        super.onCreate(savedInstanceState)
        mCallingActivity = currentActivity
    }


    fun initToolbar(toolbar: Toolbar, activityTitle: String) {
        mContext = this
        mToolbar = toolbar
        mTitle = mToolbar!!.findViewById<View>(R.id.toolbar_title) as TextView
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        updateHeaderText(activityTitle)
    }

    protected fun endScreen() {
        finish()
    }

    fun onLeftBtnPressed() {
        endScreen()
    }

    fun updateHeaderText(text: String) {
        if (mTitle != null) {
            supportActionBar!!.title = ""
            mToolbar!!.visibility = View.VISIBLE
            mTitle!!.text = text
        }
    }


//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        val itemId = item.itemId
//        when (itemId) {
//            android.R.id.home -> {
//                AppUtil.hideSoftKeyboard(mCallingActivity!!)
//                mCallingActivity!!.finish()
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }
}
