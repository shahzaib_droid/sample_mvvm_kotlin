package app.bizreview.me.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.File;

import app.bizreview.me.App;
import app.bizreview.me.R;
import app.bizreview.me.utils.AppBundles;

public class CropperActivity extends Activity {

    /*
     * Responsible for cropping image selected
     */
    // Layout
    private ImageView resultView;

    // Local
    private Uri photoUri;

    // Class Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        initComponents();
        getData();
    }

    // Helping methods
    public void initComponents() {
        resultView = (ImageView) findViewById(R.id.result_image);
    }

    public void getData() {
        if (this.getIntent() != null) {
            photoUri = this.getIntent().getParcelableExtra(
                    AppBundles.CROPPER_IMAGE.getKey());
            if (photoUri != null) {
                resultView.setImageDrawable(null);
                // resultView.setImageURI(photoUri);
                beginCrop(photoUri);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, result);
            }
        } else {
            setResult(RESULT_CANCELED, result);
            this.finish();
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(App.Companion.getAQCacheDir(), "cropped.jpg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK, result);
            this.finish();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
