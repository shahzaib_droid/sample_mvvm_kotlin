package app.bizreview.me.ui.viewModel

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.forgotPassword.ForgotPasswordResponse
import app.bizreview.me.ui.HomeActivity
import app.bizreview.me.utils.AppUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

/**
 * Created by shahzaib on 3/5/2018.
 */
class ForgotViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel {


    lateinit var mProgressDialog: ProgressDialog

    var disposable: DisposableObserver<ForgotPasswordResponse>? = null
    var response: ForgotPasswordResponse? = null

    var editTextUsernameValue: String? = null

    override fun destroy() {

        if (disposable != null) {
            if (!disposable!!.isDisposed) disposable!!.dispose()
        }
        disposable = null
        context = null
        dataListener = null
    }


    fun onClickForgot(view: View) {

        AppUtil.hideSoftKeyboard(context as Activity)
        if (editTextUsernameValue != null &&
                !editTextUsernameValue?.isEmpty()!!) {
            if (AppUtil.isEmailValid(editTextUsernameValue)) {
                forgot(editTextUsernameValue)
            } else {
                dataListener?.onMessage("Please enter valid email address")
            }
        } else {
            if (editTextUsernameValue == null ||
                    editTextUsernameValue?.isEmpty()!!) {
                dataListener?.onMessage("Please enter email address")
            }
        }
    }

    fun getUsernameEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                editTextUsernameValue = charSequence.toString()
            }

            override fun afterTextChanged(editable: Editable) {

            }
        }
    }

    // forgot api call
    fun forgot(username: String?) {

        showProgressDialog("Forgot password...", 100)
        val injector = Injector()
        val api = injector.provideApi()
        disposable = api.forgotPassword(username)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    interface DataListener {
        fun onForgotSuccess(response: ForgotPasswordResponse)
        fun onMessage(message: String)
        fun onSignInClick()
    }

    private inner class LongOperationObserver : DisposableObserver<ForgotPasswordResponse>() {

        override fun onNext(value: ForgotPasswordResponse) {
            this@ForgotViewModel.response = value
        }

        override fun onError(e: Throwable) {
            hideProgressDialog()
            dataListener?.onMessage(e.localizedMessage)
        }

        override fun onComplete() {
            if (response?.status?.equals("success", true)!!) {
                dataListener?.onForgotSuccess(response!!)

            } else {
                dataListener?.onMessage(response?.message!!)
            }
            hideProgressDialog()
        }
    }

    //Show Loader
    fun showProgressDialog(text: String, progress: Int) {
        mProgressDialog = ProgressDialog(context, R.style.AppCompatAlertDialogStyle)
        mProgressDialog.setMessage(text)
        mProgressDialog.setIndeterminate(false)
        mProgressDialog.setMax(progress)
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    //hide progress
    fun hideProgressDialog() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    fun onClickForgot(): View.OnClickListener {
        return View.OnClickListener {
            dataListener?.onSignInClick()
        }
    }

    fun startHome() {
        val intent = Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }
}