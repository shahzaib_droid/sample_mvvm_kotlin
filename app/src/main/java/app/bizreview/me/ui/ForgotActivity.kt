package app.bizreview.me.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import app.bizreview.me.R
import app.bizreview.me.data.model.forgotPassword.ForgotPasswordResponse
import app.bizreview.me.databinding.ActivityForgotBinding
import app.bizreview.me.ui.viewModel.ForgotViewModel

/**
 * A login screen that offers login via email/password.
 */
class ForgotActivity : BizReviewMeActivity(), ForgotViewModel.DataListener {

    lateinit var binding: ActivityForgotBinding
    lateinit var viewModel: ForgotViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot)
        viewModel = ForgotViewModel(this, this)
        binding.model = viewModel

    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }

    override fun onForgotSuccess(response: ForgotPasswordResponse) {
        /* val intent = Intent(this, LoginActivity::class.java)
         intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
         intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
         startActivity(intent)*/
        finish()
    }

    override fun onMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onSignInClick() {
        finish()
    }

}