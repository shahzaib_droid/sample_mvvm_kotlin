package app.bizreview.me.ui.viewModel

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.addPlace.AddPlaceResponse
import app.bizreview.me.ui.SlidingHomeActivity
import app.bizreview.me.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import okhttp3.RequestBody
import java.io.File

/**
 * Created by shahzaib on 3/13/2018.
 */
class UploadImageViewModel(var context: Context?, var dataListener: DataListener?, var isEditMode: Boolean)
    : ViewModel, ProgressRequestBody.UploadCallbacks {
    //local
    var imagePath: String? = null
    lateinit var mProgressDialog: ProgressDialog

    // disposable
    var disposableAdd: DisposableObserver<AddPlaceResponse>? = null
    var responseAdd: AddPlaceResponse? = null


    override fun destroy() {

        if (disposableAdd != null) {
            if (!disposableAdd!!.isDisposed) disposableAdd!!.dispose()
        }
        disposableAdd = null
        context = null
        dataListener = null
    }

    // onClicks
    fun getNotesEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {

            }
        }
    }


    fun onClickUpload(): View.OnClickListener {
        return View.OnClickListener {
            //            if (editTextNameValue != null && !(editTextNameValue as String).isEmpty()
//                    && editTextNotesValue != null && !(editTextNotesValue as String).isEmpty()
//                    && editTextPlaceIDValue != null && !(editTextPlaceIDValue as String).isEmpty()) {
////                    && imagePath != null && !(imagePath as String).isEmpty()) {
            // todo normal multipart upload without progress update
            uploadImage()
//             dataListener?.onUploadWithProgress(imagePath)

//            } else {
//                if (editTextNameValue == null || (editTextNameValue as String).isEmpty()) {
//                    dataListener?.onMessage("Please enter name")
//                } else if (editTextNotesValue == null || (editTextNotesValue as String).isEmpty()) {
//                    dataListener?.onMessage("Please enter notes")
//                } else if (editTextPlaceIDValue == null || (editTextPlaceIDValue as String).isEmpty()) {
//                    dataListener?.onMessage("Please select place")
////                } else if (imagePath == null || (imagePath as String).isEmpty()) {
////                    dataListener?.onMessage("Please select image")
//                }
//
//            }
        }
    }

    fun onClickImage(): View.OnClickListener {
        return View.OnClickListener {
            dataListener?.onImage()
        }
    }


    override fun onProgressUpdate(percentage: Int) {
    }

    override fun onError() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    override fun onFinish() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }


    //api
    fun uploadImage() {
        AppUtil.hideSoftKeyboard(context as Activity)
        val header = HashMap<String, String>()
        header.put("x-access-token", AppPreference.getValue(context, AppWebServices.KEY_USER_ACCESS_TOKEN))

        val body = HashMap<String, RequestBody>()
        body.put("place_id", NetworkUtility.toRequestBody("Xl4LEvt4lpudAMcb9q1vtQ"))
        body.put("name", NetworkUtility.toRequestBody("3 Blind Brothers"))
        body.put("notes", NetworkUtility.toRequestBody("Thank you for using 3 Blind Brothers, please let us know how you enjoyed our services."))
        if (imagePath != null) {
            val temp = File(imagePath)
            val fileBody = ProgressRequestBody(temp, this)
            body.put("logo\"; filename=\"image.jpg\" ", fileBody)
        }

        val injector = Injector()
        val api = injector.provideApi()

        showProgressDialog("Uploading image...", 100)
        disposableAdd = api.addPlace(header, body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(UpdateLongOperationObserver())

    }


    private inner class UpdateLongOperationObserver : DisposableObserver<AddPlaceResponse>() {

        override fun onNext(value: AddPlaceResponse) {
            responseAdd = value
        }

        override fun onError(e: Throwable) {
            dataListener?.onMessage(e.localizedMessage)
            hideProgressDialog()
        }

        override fun onComplete() {
            hideProgressDialog()
            if (responseAdd?.status?.equals("success", true)!!) {
                dataListener?.onMessage("Place added successfully")
                dataListener?.onAddSuccess(responseAdd)
            }
        }
    }

    //Show Loader
    fun showProgressDialog(text: String, progress: Int) {
        try {

            mProgressDialog = ProgressDialog(context, R.style.AppCompatAlertDialogStyle)
            mProgressDialog.setMessage(text)
            mProgressDialog.setIndeterminate(false)
            mProgressDialog.setMax(progress)
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mProgressDialog.setCancelable(false)
            mProgressDialog.show()
        } catch (e: Exception) {

        }
    }

    //hide progress
    fun hideProgressDialog() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    // inerface to communicate between model and activity
    interface DataListener {

        fun onImage()
        fun onUploadWithProgress(path : String?)
        fun onAddSuccess(response: AddPlaceResponse?)
        fun onMessage(message: String)

    }
}