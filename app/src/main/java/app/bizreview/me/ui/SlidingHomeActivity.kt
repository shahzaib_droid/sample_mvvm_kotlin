package app.bizreview.me.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import app.bizreview.me.R
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.room.AppDatabase
import app.bizreview.me.databinding.ActivityHomeBinding
import app.bizreview.me.databinding.ActivitySlidingHomeBinding
import app.bizreview.me.ui.fragments.UploadImageFragment
import app.bizreview.me.ui.viewModel.HomeViewModel
import app.bizreview.me.ui.viewModel.SlidingHomeViewModel
import app.bizreview.me.utils.AppConstants
import app.bizreview.me.utils.AppPreference
import kotlinx.android.synthetic.main.activity_sliding_home.*
import kotlinx.android.synthetic.main.app_bar_sliding_home.*

class SlidingHomeActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        SlidingHomeViewModel.DataListener {

    /*
      Activity responsible for drwaer menu
     */

    public lateinit var binding: ActivitySlidingHomeBinding
    lateinit var model: SlidingHomeViewModel
    var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sliding_home)
        model = SlidingHomeViewModel(this, this)
        binding.model = model
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.sliding_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_logout -> {
                AppPreference.saveData(this, false, AppConstants.KEY_IS_LOGGED)
                finish()
                return true
            }
            R.id.action_user -> {
                var appDataBase: AppDatabase = AppDatabase.getAppDatabase(this)
                var response: Array<LoginResponse> = appDataBase.loginDao().loadAllUsers()
                onMessage(response.get(0).data?.userinfo?.first_name as CharSequence)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_upload -> {
                onUpload()
            }
            R.id.nav_gallery -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    public fun setHeaderText(title: String) {
        supportActionBar?.title = title
    }

    private fun onUpload() {
        fragment = UploadImageFragment()
        onReplaceFragment(fragment, "upload")
    }

    fun onFragmentSelected(clear: Boolean, tag: String) {
        if (fragment != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            if (clear) {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            } else {
                fragmentTransaction.addToBackStack(tag)
            }
            fragmentTransaction.replace(R.id.fragment_container, fragment)
            fragmentTransaction.commit()
        }
    }

    fun onReplaceFragment(fragment: Fragment?, Tag: String) {
        if (fragment != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragment,
                    Tag)
//            fragmentTransaction.addToBackStack(Tag)
            fragmentTransaction.commit()
        }
    }

    fun onMessage(message: CharSequence) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

}
