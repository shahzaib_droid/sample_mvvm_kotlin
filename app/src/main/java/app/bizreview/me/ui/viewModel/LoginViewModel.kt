package app.bizreview.me.ui.viewModel

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import app.bizreview.me.App
import app.bizreview.me.R
import app.bizreview.me.data.Injector
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.model.updateProfile.UpdateProfileResponse
import app.bizreview.me.ui.ForgotActivity
import app.bizreview.me.ui.HomeActivity
import app.bizreview.me.ui.NewWebviewActivity
import app.bizreview.me.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver


/**
 * Created by shahzaib on 3/5/2018.
 */
class LoginViewModel(var context: Context?, var dataListener: DataListener?) : ViewModel, ProgressRequestBody.UploadCallbacks {


    lateinit var mProgressDialog: ProgressDialog

    var disposable: DisposableObserver<LoginResponse>? = null
    var response: LoginResponse? = null

    var editTextUsernameValue: String? = "trevyn.meyer@gmail.com"
    var editTextPasswordValue: String? = "8eastMode!!"

    override fun destroy() {

        if (disposable != null) {
            if (!disposable!!.isDisposed) disposable!!.dispose()
        }
        disposable = null
        context = null
        dataListener = null
    }

    fun onClickForgot(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(context, ForgotActivity::class.java)
            context?.startActivity(intent)
        }
    }

    fun onClickSignUp(): View.OnClickListener {
        // on sign up click opening website in webview
        return View.OnClickListener {
            val intent = Intent(context, NewWebviewActivity::class.java)
            val bundle = Bundle()
            bundle.putString(AppBundles.TITLE.key, AppConstants.URL_APPLY_TITLE)
            bundle.putString(AppBundles.URL.key, AppConstants.URL_APPLY)
            intent.putExtras(bundle)
            context?.startActivity(intent)
        }
    }


    fun onClickLogin(view: View) {
        //hiding keyboard
        AppUtil.hideSoftKeyboard(context as Activity)
        // check for email and password values
        if (editTextUsernameValue != null &&
                !editTextUsernameValue?.isEmpty()!! &&
                editTextPasswordValue != null &&
                !editTextPasswordValue?.isEmpty()!!) {
            if (AppUtil.isEmailValid(editTextUsernameValue)) {
                login(editTextUsernameValue, editTextPasswordValue)
            } else {
                dataListener?.onMessage("Please enter valid email address")
            }
        } else {
            if (editTextUsernameValue == null ||
                    editTextUsernameValue?.isEmpty()!!) {
                dataListener?.onMessage("Please enter email address")
            } else if (editTextPasswordValue == null ||
                    editTextPasswordValue?.isEmpty()!!) {
                dataListener?.onMessage("Please enter password")
            }
        }
    }

    fun getUsernameEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                editTextUsernameValue = charSequence.toString()
            }

            override fun afterTextChanged(editable: Editable) {

            }
        }
    }

    fun getPasswordEditTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
                editTextPasswordValue = charSequence.toString()
            }

            override fun afterTextChanged(editable: Editable) {

            }
        }
    }

    fun login(username: String?, password: String?) {

        showProgressDialog("Signing in...", 100)
        val injector = Injector()
        val api = injector.provideApi()
        disposable = api.login(username, password, true)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    // interface to commmunicate between view model and activity
    interface DataListener {
        fun onLoginSuccess(response: LoginResponse)
        fun onMessage(message: String)
    }

    private inner class LongOperationObserver : DisposableObserver<LoginResponse>() {

        override fun onNext(value: LoginResponse) {
            this@LoginViewModel.response = value
        }

        override fun onError(e: Throwable) {
            hideProgressDialog()
            dataListener?.onMessage(e.localizedMessage)
        }

        override fun onComplete() {
            if (response?.status?.equals("success", true)!!) {
                AppPreference.saveValue(context, editTextPasswordValue, AppConstants.KEY_USER_PASSWORD)

                dataListener?.onLoginSuccess(response!!)
            } else {
                dataListener?.onMessage(response?.message!!)
            }
            hideProgressDialog()
        }
    }

    //Show Loader
    fun showProgressDialog(text: String, progress: Int) {
        mProgressDialog = ProgressDialog(context, R.style.AppCompatAlertDialogStyle)
        mProgressDialog.setMessage(text)
        mProgressDialog.setIndeterminate(false)
        mProgressDialog.setMax(progress)
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    //hide progress
    fun hideProgressDialog() {
        if (mProgressDialog?.isShowing) {
            mProgressDialog.hide()
        }
    }

    fun startHome() {
        val intent = Intent(context, HomeActivity::class.java)
        context?.startActivity(intent)
    }


    override fun onProgressUpdate(percentage: Int) {
    }

    override fun onError() {
    }

    override fun onFinish() {
        dataListener?.onMessage("image upload finished")
    }

}
