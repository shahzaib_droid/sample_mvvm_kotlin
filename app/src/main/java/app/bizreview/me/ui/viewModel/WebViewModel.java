package app.bizreview.me.ui.viewModel;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableInt;
import android.view.View;

public class WebViewModel implements ViewModel {

    private static final String TAG = "WebViewModel";
    private ObservableInt rootLayout;
    private ProgressDialog mProgressDialog;
    private Context context;

    public WebViewModel(Context context) {
        this.context = context;
        rootLayout = new ObservableInt(View.VISIBLE);
    }


    private void showProgressDialog(String text, int progress) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(text);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(progress);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    //Override
    @Override
    public void destroy() {
        context = null;
    }

}
