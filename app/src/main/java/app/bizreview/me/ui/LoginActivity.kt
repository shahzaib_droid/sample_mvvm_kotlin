package app.bizreview.me.ui

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import app.bizreview.me.R
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.room.AppDatabase
//import app.bizreview.me.data.room.DatabaseInitializer
import app.bizreview.me.data.room.MyDao
import app.bizreview.me.databinding.ActivityLoginBinding
import app.bizreview.me.ui.viewModel.LoginViewModel
import app.bizreview.me.utils.AppConstants
import app.bizreview.me.utils.AppPreference
import app.bizreview.me.utils.AppWebServices

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : BizReviewMeActivity(), LoginViewModel.DataListener {

    lateinit var binding: ActivityLoginBinding
    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = LoginViewModel(this, this)
        binding.model = viewModel
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.destroy()
    }

    override fun onLoginSuccess(response: LoginResponse) {

        var appDataBase : AppDatabase = AppDatabase.getAppDatabase(this)
        response.uid = 0
        appDataBase.loginDao().deleteUsers(response)
        appDataBase.loginDao().insertLoginResponse(response)

        AppPreference.saveData(this, true, AppConstants.KEY_IS_LOGGED)
        AppPreference.saveValue(this, response?.data?.token, AppWebServices.KEY_USER_ACCESS_TOKEN)
        AppPreference.saveValue(this, response?.data?.expires.toString(), AppConstants.KEY_USER_TOKEN_EXPIRE)
        AppPreference.saveValue(this, response.data?.userinfo?.first_name, AppConstants.KEY_FIRST_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.last_name, AppConstants.KEY_LAST_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.fullname, AppConstants.KEY_FULL_NAME)
        AppPreference.saveValue(this, response.data?.userinfo?.email, AppConstants.KEY_EMAIL)
        AppPreference.saveValue(this, response.data?.userinfo?.id, AppConstants.KEY_USER_ID)

        viewModel.startHome()
        finish()
    }

    override fun onMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

}