package app.bizreview.me.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import app.bizreview.me.R;
import app.bizreview.me.databinding.ActivityWebviewNewBinding;
import app.bizreview.me.ui.viewModel.WebViewModel;
import app.bizreview.me.utils.AppBundles;


public class NewWebviewActivity extends BizReviewMeActivity {

    /*
     Activity responsible for webview to open Signup page
     */

    //Local
    private Context mContext;
    private String ACTIVITY_TITLE = "";
    private String url;
    private Toolbar mToolbar;
    //Data binding
    private ActivityWebviewNewBinding activityWebViewBinding;
    private TextView mTitle;
    private WebViewModel webViewModel;

    //Class Methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        initActivity();
        getData();
        initToolbar(activityWebViewBinding.toolbarHeader, ACTIVITY_TITLE);
    }

    //Helping Methods
    public void initActivity() {
        activityWebViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_webview_new);

        webViewModel = new WebViewModel(this);
        activityWebViewBinding.setModel(webViewModel);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateHeadeText(String text) {
        if (mTitle != null) {
            getSupportActionBar().setTitle("");
            mToolbar.setVisibility(View.VISIBLE);
            mTitle.setText(text);
        }
    }

    public void getData() {
        if (this.getIntent() != null) {
            Bundle bundle = this.getIntent().getExtras();
            url = bundle.getString(AppBundles.URL.getKey());
            ACTIVITY_TITLE = bundle.getString(AppBundles.TITLE.getKey());
            loadUrl();
        }
    }

    public void loadUrl() {
        activityWebViewBinding.wvHome.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                activityWebViewBinding.progress.setVisibility(View.VISIBLE);
                activityWebViewBinding.progress.setProgress(progress);
                if (progress == 100) {
                    activityWebViewBinding.progress.setProgress(0);
                    activityWebViewBinding.progress.setVisibility(View.GONE);
                }
            }
        });
        activityWebViewBinding.wvHome.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.setInitialScale(0);
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
        activityWebViewBinding.wvHome.getSettings().setLoadWithOverviewMode(true);
        activityWebViewBinding.wvHome.getSettings().setBuiltInZoomControls(true);
        activityWebViewBinding.wvHome.getSettings().setJavaScriptEnabled(true);
        activityWebViewBinding.wvHome.loadUrl(url);
    }


    @Override
    public void onBackPressed() {
        if (activityWebViewBinding.wvHome.canGoBack()) {
            activityWebViewBinding.wvHome.goBack();
        } else {
            finish();
        }
    }
}
