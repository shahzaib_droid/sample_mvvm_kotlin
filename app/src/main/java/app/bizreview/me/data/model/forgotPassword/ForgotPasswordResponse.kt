package app.bizreview.me.data.model.forgotPassword

/**
 * Created by shahzaib on 4/6/2018.
 */

data class ForgotPasswordResponse(
		var data: Data?,
		var status: String?,
		var message: String?
)

data class Data(
		var email: String?
)