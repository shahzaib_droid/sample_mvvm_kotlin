package app.bizreview.me.data.model.info

/**
 * Created by shahzaib on 4/11/2018.
 */

data class InfoResponse(
        var data: Data?,
        var status: String?,
        var message: String?
)

data class Data(
        var id: String?,
        var email: String?,
        var stripe_id: Any?,
        var `package`: Package?,
        var created_on: String?,
        var last_login: String?,
        var active: String?,
        var first_name: String?,
        var last_name: String?,
        var phone: String?,
        var groups: List<String?>?,
        var details: Boolean?,
        var fullname: String?
//        var billing: List<Any?>?
)

data class Package(
        var id: String?,
        var name: String?,
        var places: String?,
        var sms: String?,
        var mms: String?,
        var price: String?,
        var stripe_subscription_id: String?,
        var status: String?,
        var description: String?
)