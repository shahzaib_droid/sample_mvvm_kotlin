package app.bizreview.me.data.model.sentReviews

/**
 * Created by shahzaib on 4/10/2018.
 */

data class SentReviewsResponse(
		var message: String?,
		var data: MutableList<Data?>?,
		var status: String?
)

data class Data(
		var id: String?,
		var user_id: String?,
		var place_id: String?,
		var response: String?,
		var to: String?,
		var request: String?,
		var rating: String?,
		var status: String?,
		var timestamp: String?,
		var timestamp_responded: String?,
		var key: String?,
		var place: String?
)