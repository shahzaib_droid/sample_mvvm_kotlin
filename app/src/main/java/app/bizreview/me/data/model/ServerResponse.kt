package app.bizreview.me.data.model

/**
 * Created by shahzaib on 3/5/2018.
 */
data class ServerResponse(
        var status: String?,
        var message: String?
)