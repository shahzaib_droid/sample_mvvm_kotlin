package app.bizreview.me.data.model.addPlace

/**
 * Created by shahzaib on 4/4/2018.
 */

data class AddPlaceResponse(
		var data: Data?,
		var status: String?,
		var message: String?
)

data class Data(
		var id: Int?
)