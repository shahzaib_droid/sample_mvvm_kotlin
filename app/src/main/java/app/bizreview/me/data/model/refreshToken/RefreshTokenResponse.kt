package app.bizreview.me.data.model.refreshToken

/**
 * Created by shahzaib on 3/29/2018.
 */

data class RefreshTokenResponse(
		var token: String?,
		var refreshed: Boolean?,
		var expires: Int?,
		var age: Int?,
		var message: String?,
		var status: String?
)