package app.bizreview.me.data.model.login

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Created by shahzaib on 3/28/2018.
 */
@Entity
data class LoginResponse(
        @PrimaryKey(autoGenerate = true)
        var uid: Int?,
        @Embedded
        var data: Data?,
        var status: String?,
        var message: String?
)

public data class Data(
        @PrimaryKey
        var token: String?,
        var expires: Int?,
        var refresh: Int?,
        @Embedded
        var userinfo: Userinfo?
)

public data class Userinfo(
        @PrimaryKey
        var id: String? = "",
        var email: String? = "",
        var created_on: String? = "",
        var last_login: String? = "",
        var active: String? = "",
        var first_name: String? = "",
        var last_name: String? = "",
        var phone: String? = "",
//        var groups: List<String?>? = mutableListOf(),
        var details: Boolean?,
        var fullname: String? = ""
)
