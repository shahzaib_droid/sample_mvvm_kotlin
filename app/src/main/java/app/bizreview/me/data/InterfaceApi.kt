package app.bizreview.me.data


import app.bizreview.me.data.model.addPlace.AddPlaceResponse
import app.bizreview.me.data.model.forgotPassword.ForgotPasswordResponse
import app.bizreview.me.data.model.info.InfoResponse
import app.bizreview.me.data.model.login.LoginResponse
import app.bizreview.me.data.model.refreshToken.RefreshTokenResponse
import app.bizreview.me.data.model.sentReviews.SentReviewsResponse
import app.bizreview.me.utils.AppWebServices
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Created by shahzaib on 10/06/17.
 */

// interface containing all API Methods
interface InterfaceApi {

    @FormUrlEncoded
    @POST(AppWebServices.API_USER_LOGIN)
    fun login(@Field("identity") userName: String?, @Field("password") password: String?, @Field("userinfo") userinfo: Boolean?): Observable<LoginResponse>

    @FormUrlEncoded
    @POST(AppWebServices.API_FORGOT_PASSWORD)
    fun forgotPassword(@Field("email") email: String?): Observable<ForgotPasswordResponse>

    @FormUrlEncoded
    @POST(AppWebServices.API_AUTO_REFRESH_TOKEN)
    fun refreshToken(@Field("token") token: String?, @Field("add") add: Double?, @Field("force") force: Boolean?): Observable<RefreshTokenResponse>



    @GET(AppWebServices.API_INFO)
    fun info(@HeaderMap token: Map<String, String>, @Query("user_id") userID : Int?): Observable<InfoResponse>

    @GET(AppWebServices.API_SENT_REVIEWS)
    fun getSentReviews(@HeaderMap token: Map<String, String>): Observable<SentReviewsResponse>

    @Multipart
    @POST(AppWebServices.API_ADD_PLACE)
    fun addPlace(@HeaderMap headers: Map<String, String>,
                 @PartMap body: Map<String, @JvmSuppressWildcards RequestBody>): Observable<AddPlaceResponse>

}