package app.bizreview.me.data.model.contacts

/**
 * Created by shahzaib on 4/10/2018.
 */
data class Contact (
        var name : String?,
        var phone : String?
)
