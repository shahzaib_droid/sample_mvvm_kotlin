package app.bizreview.me.data.room

import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import android.content.Context
import app.bizreview.me.data.model.login.LoginResponse


/**
 * Created by shahzaib on 6/27/2018.
 */

// usage of room for Sqlite database (Google recommended)

@Database(entities = arrayOf(LoginResponse::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    // method to get Dao interface
    abstract fun loginDao(): MyDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase::class.java, "user-database")
                        // allow queries on the main thread.
                        // Don't do this on a real app! See PersistenceBasicSample for an example.
                        .allowMainThreadQueries()
                        .build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}