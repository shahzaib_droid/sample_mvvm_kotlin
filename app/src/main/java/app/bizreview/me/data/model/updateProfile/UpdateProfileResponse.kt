package app.bizreview.me.data.model.updateProfile

/**
 * Created by shahzaib on 3/5/2018.
 */

data class UpdateProfileResponse(
		var message: String?,
		var data: Data?,
		var stats: Stats?,
		var status: String?
)

data class Stats(
		var image_process: Boolean?,
		var image_url: String?,
		var info_update: Boolean?
)

data class Data(
		var avatar: String?,
		var first_name: String?,
		var last_name: String?,
		var phone: String?,
		var updated: String?,
		var profile_picture: String?
)