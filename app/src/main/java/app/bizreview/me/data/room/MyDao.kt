package app.bizreview.me.data.room

import android.arch.persistence.room.*
import app.bizreview.me.data.model.login.LoginResponse


/**
 * Created by shahzaib on 6/27/2018.
 */

// Dao interface for database operation methods
@Dao
interface MyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public fun insertLoginResponse(response: LoginResponse)

    @Delete
    public fun deleteUsers(response: LoginResponse)

    @Query("SELECT * FROM LoginResponse")
    public fun loadAllUsers(): Array<LoginResponse>
}