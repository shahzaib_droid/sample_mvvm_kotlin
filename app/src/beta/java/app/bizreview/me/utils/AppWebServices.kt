package app.bizreview.me.utils

/**
 * Created by shahzaib on 3/5/2018.
 */
object AppWebServices{

    // url constants for api
    const val BASE_URL = "https://app.bizreview.me/"
    const val API_USER_LOGIN = "services/auth/login"
    const val KEY_USER_ACCESS_TOKEN = "user_access_token"
    const val API_UPDATE_TEAM = "services/teams/update"
    const val API_PLACES_ALL = "services/places/all"
    const val API_AUTO_REFRESH_TOKEN = "services/auth/refresh"
}