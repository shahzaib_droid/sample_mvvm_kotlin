package app.bizreview.me.utils

/**
 * Created by shahzaib on 3/5/2018.
 */
object AppWebServices{

    // Api url constants for staging

    const val BASE_URL = "https://app.bizreview.me/"
    const val API_USER_LOGIN = "services/auth/login"
    const val KEY_USER_ACCESS_TOKEN = "user_access_token"
    const val API_UPDATE_TEAM = "services/teams/update"
    const val API_PLACES_ALL = "services/places/all"
    const val API_AUTO_REFRESH_TOKEN = "services/auth/refresh"
    const val API_ADD_PLACE = "services/places/add"
    const val API_UPDATE_PLACE = "services/places/update"
    const val API_DELETE_PLACE = "services/places/delete"
    const val API_REVIEW_PLACE = "services/places/reviews"
    const val API_FORGOT_PASSWORD = "services/accounts/forgot_password"
    const val API_UPDATE_PASSWORD = "services/accounts/update"
    const val API_SENT_REVIEWS = "services/reviews/lists"
    const val API_REQUEST_REVIEW = "services/reviews/request"
    const val API_STATS = "services/stats/all"
    const val API_INFO = "services/accounts/info"


}